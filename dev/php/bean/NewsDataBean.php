<?php
/**
 * NewsDataBean.php : DataBean utilis� pour les news du jeu
 *
 * Date    : 31/05/2009
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2008-2009 Philippe & Thierry Bousquet.
 *
 * Ce logiciel est distribu� selon les termes de la GNU General Public License v2.
 */

class NewsDataBean {
 
  private $news;

  function __construct() {
	  $this->news = array();
  }

  function setNews($news) {
    $this->news = $news;
  }
  function getNewsAt($idx) {
    return $this->news[$idx];
  }
  function getNbNews() {
    return sizeof($this->news);
  }
  
}
?>
