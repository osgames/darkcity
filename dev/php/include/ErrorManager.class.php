<?php
/**
 * ErrorManager.class.php : Manager pour les messages d'erreurs
 *
 * Date    : 14/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribu� selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */

class ErrorManager {
  
	private $message = "";

	/**
	 * isError - y a t'il une message d'erreur poitionn�
	 */ 
	function isError() {
		return ($this->message != "");
	}
 
	/**
	 * setError - positionne un message d'erreur
	 */ 
	function setError($msg) {
		$this->message = $msg;
	}

	/**
	 * getError - r�cup�re le message d'erreur
	 */ 
	function getError() {
		return $this->message;
	}
}  
?>
