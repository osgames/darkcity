<?php
/**
 * DateManager.class.php : Gestionaire de Date
 *
 * Date    : 17/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 * 
 * Ce logiciel est distribu� selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */

class DateManager {

	function getCurrentDate() {
		$result = mysql_query('SELECT UNIX_TIMESTAMP()');	
		$row = mysql_fetch_array($result);
		return $row[0];
	}

	function getFutureDate($secondes) {
		return $this->getCurrentDate() + $secondes;
	}

	function getPastDate($secondes) {
		return $this->getCurrentDate() - $secondes;
	}
  
	function compteRebour($date) {
		$timer = $this->getCurrentDate();	
		return ($date >= $timer ? $date - $timer : 0);
	}

	function mySQLToDate($datetime) {
		return strtotime($datetime);
	}

	function dateToString($date) {
		$dateStr = "";
		if (date("Y-m-d", $this->getCurrentDate()) != date("Y-m-d", $date)) {
			$dateStr = " le " . date("d/m/Y", $date) . ' ';
		}
		return $dateStr . ' &agrave; '. date("H:i:s", $date);
	}
    
	function getDateLimite() {
		$dateStr = "";
		return strtotime(date("Y-m-d")." 23:59:59");
	}

	function getDureeToString($duree) {
		$heures = floor($duree / 60);
		$minutes = $duree - ($heures * 60);
		return (($heures > 0) ? $heures."h " : "").(($minutes > 0) ? $minutes."m" : ""); 
	}
}

?>
