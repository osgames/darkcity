<?php
/**
 * Config.class.php : Classe de configuration de DarkCity 2
 *
 * Date    : 09/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribu� selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */
class Config {

	/** login pour la base de donn�es */
	private $databaseUser;
	
	/** mot de passe de connexion � la base de donn�es */
	private $databasePassword;
	
	/** serveur de base de donn�es */
	private $databaseHost;
	
	/** nom de la base de donn�es */
	private $databaseName; 
	
	/** version du jeu */
	private $gameVersion;
	
	/**
	 * Constructeur par defaut
	 */
	function __construct() {
		$this->databaseUser = "darkcity2" ;
		$this->databaseHost = "127.0.0.1";
		$this->databaseName = "darkcity2"; 
		$this->databasePassword = "motpasse";
		$this->gameVersion = "2.0.0";
	}
	
	/**
	 * getter databaseUser
	 */ 
	function getDatabaseUser() {
		return $this->databaseUser;
	}
	
	/**
	 * getter databasePassword
	 */ 
	function getDatabasePassword() {
		return $this->databasePassword;
	}
	
	/**
	 * getter databaseHost
	 */ 
	function getDatabaseHost() {
		return $this->databaseHost;
	}
	
	/**
	 * getter databaseName
	 */ 
	function getDatabaseName() {
		return $this->databaseName;
	}
	
	/**
	 * getter gameVersion
	 */ 
	function getGameVersion() {
		return $this->gameVersion;
	}
	
}	
?>
