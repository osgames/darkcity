<?php
/**
 * InfoManager.php : Manager pour les messages d'infos
 *
 * Date    : 31/05/2009
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2008-2009 Philippe & Thierry Bousquet.
 *
 * Ce logiciel est distribu� selon les termes de la GNU General Public License v2.
 */

 class InfoManager {
  
  var $message = "";
  
  // Constructeur
  function InfoManager() {
    $session="";
  }
  
  // Verification de la session
  function isInfo() {
    if ($this->message != "") {
        return true;
    }
    else {
      return false;
    }
  }

  
  function setMessage($msg) {
    $this->message = $msg."<br/>";
  }

  function razMessage() {
    $this->message = "";
  }

  function addMessage($msg) {
    $this->message = $this->message . $msg . "<br/>";
  }

  function getMessage() {
      return $this->message;
  }
}  
?>