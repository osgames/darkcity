<?php
/**
 * PerformanceManager.class.php : Calculateur de performance
 *
 * Date    : 09/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribu� selon les termes de la GNU General Public License v2.
 * License : http://www.gnu.org/copyleft/gpl.html
 */

class PerformanceManager {

	/** timer de d�part */
	private $timer;
	
	/** logManger */
	private $logManager;

	/** page */
	private $page;
	
	/**
	 * Foction priv�e utime
	 */
	private function utime() {
		$time = explode( " ", microtime());
		$usec = (double)$time[0];
		$sec = (double)$time[1];
		return $sec + $usec;
	}

	/**
	 * constructeur
	 */
	 function __construct() {
		 $logManager = new LogManager(LogManager->INFO);
	 }
	  
	/**
	 * start - D�marrer le timer
	 */ 
	function start() {
		$this->timer = $this->utime();
	}
  
	/**
	 * stop - arr�ter le timer
	 */ 
	function stop() {
		$run = 0;
		if ($this->timer != 0) {
			$end = $this->utime();
			$run = $end - $this->timer;
			$this->timer = 0;
			$run = substr($run, 0, 5);
			$logManager->message($page . " => " . $run . "ms.", LogManager->INFO);
		}
		return $run;
	}
  
}

?>
