<?php
/**
 * DiceManager.class.php : Manager pour le jet de D�s
 *
 * Date    : 11/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribu� selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */

 class DiceManager {

	/**
	 * roll - lancer des des
	 */ 
	function roll($level, $bonus) {
		return round(($level+$bonus) - rand(1, 100));
	}
   
 }
?>
