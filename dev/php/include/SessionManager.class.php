<?php
/**
 * SessionManager.clas.php : Manager pour la session
 *
 * Date    : 09/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribu� selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */

 class SessionManager {

	/**
	 * Constructeur
	 */ 
	function __construct() {
		if (!isset($_SESSION) || !isset($_SESSION["user_name"])) {
			start();
		}
	}
	
	/**
	 * start - d�marrer une session
	 */ 
	function start() {
		session_start();
		
	}

	/**
	 * close - fermer la session
	 */ 
	function close() {
		$_SESSION = array();
	}

	/**
	 * restart - red�marrer une session
	 */
	function restart() {
		$this->close();
		$this->start();
	}
   
   /**
    * parseUrl - Support Session pour les urls
    */ 
	function parseURL($url,$vars="") {
		return $url."?".session_name()."=".session_id()."&amp;date=".microtime().($vars != "" ? "&amp;".$vars : ""); 
	}

	/**
	 * save - Sauvegarde d'une variable
	 */ 
	function save($name,$value) {
		$_SESSION[$name]=serialize($value);
	}
   
	/**
	 * load - Charger une variable sauvegard�e
	 */ 
	function load($name) {
		if (isset($_SESSION[$name])) return unserialize($_SESSION[$name]);
		else return "";
	}
    
    /**
     * getId - retourne l'id de la session
     */   
	function getId() {
		return session_id();
	}
   
 }
?>
