<?php
/**
 * LogManager.class.php : Manager pour les messages de logs
 *
 * Date    : 09/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribu� selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */

class LogManager {
  
/*
 * 	public $DEBUG = 4;
	public $WARNING = 3;
	public $INFO = 2;
	public $ERROR = 1;
	public $NONE = 0;
	private $ID = { 1=>"[ERROR]", 2=>"[INFO]", 3=>["WARNING"], 4=>["DEBUG"] };
	/** fichier de logs */
	private $fichier;

	/** log level */
	private $level;
  
	function __construct($_fichier="logs/out.log", $_level=4) {
		$this->fichier = $_fichier;
		$this->level = $_level;
	}
  
  function getLevel() {
    return $this->level;
  }
  
  function setLevel($level) {
    $this->level = $level;
  }

  function message($message,$level=4) {
    if ($level <= $this->getLevel()) {
    	$fd = fopen($this->fichier, "a");
    	fwrite($fd, date("Y-m-d H:i:s")." ".$message."\n"); 
    	fclose($fd);
    }
  }
}
?>
