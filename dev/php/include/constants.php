<?php
/**
 * constants.php : Constantes de DarkCity 2
 *
 * Date    : 17/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribué selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */  

	$MINUTE = 60;
    $VERSION = "2.0.0";
    $COPY = "Copyright &copy; 2012 - ".(date("Y")!="2012" ? date("Y")." " : " ")."Philippe Bousquet<br/>Ce logiciel est distribu&eacute; selon les terme de la licence GNUGeneral Public License v3.";
?>
