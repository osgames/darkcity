<?php
/**
 * SkillManager.class.php : Manager pour les compétesnces du jeu
 *
 * Date    : 11/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribué selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */

 class SkillManager {

	/**
	 * Le dès pour les lancés
	 */ 
	private $dice;
	
	/**
	 * Tableau des caracteristiques
	 */
	public $caracs = array(
		"PER" => array(
			"TITLE" => "Perception",
			"DESC" => "Cette caractéristique est votre aptitude à percevoir votre environnement. Elle est utile pour les recherches, les fouilles, ou déjouer les embuscades."
		),
		"REF" => array(
			"TITLE" => "Réflexes",
			"DESC" => "Cette caractéristique combine à la fois votre dextérité et votre coordination physique. Cette compétence est très utile lors de vos combats."
		),
		"CON" => array(
			"TITLE" => "Constitution",
			"DESC" => "Cette caractéristique combine votre force, votre endurance. Lorsque votre condition physique est mise à l’épreuve c’est cette caractéristique qui sera utilisée."
		),
		"INT" => array(
			"TITLE" => "Intelligence",
			"DESC" => "Cette caractéristique mesure votre aptitude à cerner et résoudre divers problèmes. L’intelligence est très importante pour effectuer des enquêtes, ou résoudre des énigmes."
		),
		"EMP" => array(
			"TITLE" => "Emphatie",
			"DESC" => "L’empathie, est votre niveau d’humanité. Elle définit votre connaissance de l’être humain ainsi que vos relations avec les autres. Si vous désirez marchander, faire passe vos idées ou détecter les mensonges des autres, ne négligez pas celle-ci."
		),
		"TECH" => array(
			"TITLE" => "Technique",
			"DESC" => "Cette caractéristique mesure votre aisance avec la technologie, que ce soit pour utiliser des objets techniques, réparer ou fabriquer des objets. Si vous avez l’âme créatrice misez sur cette caractéristique."
		)
	);
	 
	/**
	 * Tableau de compétences
	 */
	public $skills = array(
		 "armurier"=> array( 
			 "TITLE"=>"Armurier",
			 "DESC"=>"Cette compétence représente votre capacité à réparer ou fabriquer des armes et armures",
			 "CARAC"=> array(
				"TECH"=>60,
				"INT"=>40
			 )
		 ),
		 "mecanique"=> array( 
			 "TITLE"=>"Mécanique",
			 "DESC"=>"Cette compétence représente votre capacité à réparer ou fabriquer des véhicules",
			 "CARAC"=> array(
				"TECH"=>100
			 )
		 ),
		 "medecine"=> array( 
			 "TITLE"=>"Médecine",
			 "DESC"=>"Cette compétence représente votre capacité à soigner les gens, elle représente aussi votre capacité à fabriquer des médicaments et drogues",
			 "CARAC"=> array(
				"TECH"=>60,
				"EMP"=>40
			 )
		 ),
		 "cybernetique"=> array( 
			 "TITLE"=>"Cybernétique",
			 "DESC"=>"Grâce à cette compétence vous serez en mesure de réparer ou concevoir du matériel cybernétique",
			 "CARAC"=> array(
				"TECH"=>50,
				"INT"=>30,
				"EMP"=>20
			 )
		 ),
		 "esquive"=> array( 
			 "TITLE"=>"Esquive",
			 "DESC"=>"Lors des combats au corps à corps cette compétence vous permet de vous faire toucher moins souvent",
			 "CARAC"=> array(
				"REF"=>100
			 )
		 ),
		 "melee"=> array( 
			 "TITLE"=>"Mêlée",
			 "DESC"=>"Il s’agit de l’art combattre au corps a corps que ce soit avec une arme blanche (tel un couteau) ou simplement à mains nues",
			 "CARAC"=> array(
				"REF"=>60,
				"CON"=>40
			 )
		 ),
		 "tir"=> array( 
			 "TITLE"=>"Tir",
			 "DESC"=>"Il s’agit de votre capacité à utiliser les armes à feu, que ce soit un pistolet ou un fusil à pompe",
			 "CARAC"=> array(
				"REF"=>60,
				"PER"=>40
			 )
		 ),
		 "pilotage"=> array( 
			 "TITLE"=>"Pilotage",
			 "DESC"=>"Cette compétence vous permet de conduire tout type de véhicule : moto, voiture mais également AV ou van",
			 "CARAC"=> array(
				"REF"=>60,
				"TECH"=>40
			 )
		 ),
		 "endurance"=> array( 
			 "TITLE"=>"Endurance",
			 "DESC"=>"Il s’agit de la capacité à supporter la douleur, les épreuves, la fatigue",
			 "CARAC"=> array(
				"CON"=>100
			 )
		 ),
		 "athletisme"=> array( 
			 "TITLE"=>"Endurance",
			 "DESC"=>"Il s’agit de votre capacité a faire une activité sportive, course à pied, natation, etc.",
			 "CARAC"=> array(
				"CON"=>60,
				"REF"=>40
			 )
		 ),
		 "analyse"=> array( 
			 "TITLE"=>"Analyse",
			 "DESC"=>"Cette compétence vous permet de réfléchir à la meilleure façon d’effectuer une tache avant de la réaliser",
			 "CARAC"=> array(
				"INT"=>100
			 )
		 ),
		 "securite"=> array( 
			 "TITLE"=>"Sécurité",
			 "DESC"=>"Il s’agit de votre capacité à percer les coffres forts ou à pénétrer les réseaux informatiques afin de les pirater ou de les protéger",
			 "CARAC"=> array(
				"INT"=>60,
				"TECH"=>40
			 )
		 ),
		 "enquete"=> array( 
			 "TITLE"=>"Enquête",
			 "DESC"=>"Cette compétence vous permet d’aller dans la rue en quête d’indices, de déceler les mauvais coups en préparation",
			 "CARAC"=> array(
				"INT"=>50,
				"PER"=>30,
				"EMP"=>20
			 )
		 ),
		 "discretion"=> array( 
			 "TITLE"=>"Discrétion",
			 "DESC"=>"Cette compétence vous permet de vous cacher, ou de faire un mauvais coup en toute discrétion, cela peut vous sauver la vie",
			 "CARAC"=> array(
				"INT"=>60,
				"REF"=>40
			 )
		 ),
		 "bibliotheque"=> array( 
			 "TITLE"=>"Bibliothèque",
			 "DESC"=>"Grâce à cette compétence, vous pourrez apprendre et évoluer dans certaines compétences en lisant des livre, comme quoi la lecture a du bon dès fois",
			 "CARAC"=> array(
				"INT"=>100
			 )
		 ),
		 "perception"=> array( 
			 "TITLE"=>"Perception",
			 "DESC"=>"Cette compétence vous permet de détecter une personne tentant de vous tendre une embuscade, ou détecter un point lumineux apparaissant subitement sur un de vos amis.",
			 "CARAC"=> array(
				"PER"=>100
			 )
		 ),
		 "initiative"=> array( 
			 "TITLE"=>"Initiative",
			 "DESC"=>"Cette compétence mesure votre réactivité vous permettant d’agir avant les autres, tirer en premier peut avoir du bon",
			 "CARAC"=> array(
				"PER"=>60,
				"REF"=>40
			 )
		 ),
		 "pistage"=> array( 
			 "TITLE"=>"Pistage",
			 "DESC"=>"Cette compétence vous permet de retrouver la trace de quelqu'un. D’accord on sait que c’est Mickaël qui a braqué la petite vielle, mais où est-ce qu’il se cache ?",
			 "CARAC"=> array(
				"PER"=>60,
				"INT"=>40
			 )
		 ),
		 "strategie"=> array( 
			 "TITLE"=>"Stratégie",
			 "DESC"=>"Cette compétence vous permet d’analyser la situation avant un combat et de déterminer la meilleure stratégie de combat et donc de donner a vous et vos amis un avantage quant à l’issue de l’affrontement",
			 "CARAC"=> array(
				"PER"=>50,
				"INT"=>30,
				"EMP"=>20
			 )
		 ),
		 "marchandage"=> array( 
			 "TITLE"=>"Marchandage",
			 "DESC"=>"L’argent est important, et cette compétence vous permet de négocier les tarifs avec les différents marchants du jeu. Que ce soit pour acheter ou bien vendre un objet.",
			 "CARAC"=> array(
				"EMP"=>100
			 )
		 ),
		 "enseignement"=> array( 
			 "TITLE"=>"Discrétion",
			 "DESC"=>"Cette compétence mesure votre capacité à apprendre aux autres votre savoir faire. Par exemple comment enseigner à Mickaël comment réparer une fuite d’huile ou à Johnny comment tenir un pistolet",
			 "CARAC"=> array(
				"EMP"=>60,
				"INT"=>40
			 )
		 )
	 ); 
	 
	 /**
	  * Contructeur
	  */
	 function __construct() {
		 $this->dice = new DiceManager();
	 }
	   
	 function getSkill($id) {
		 return $this->caracs[$id];
	 }  
	/**
	 * roll - lancer des des
	 */ 
	function test_skill($skillName, $skills, $inventory) {
		//TODO 
	}	
   
 }

class Skill {
	
	private $_id;
	private $_values;
	
	function __construct($id) {
		$_id = $id;
		$_values = SkillManager::getSkill($id);
	}
	
}
?>
