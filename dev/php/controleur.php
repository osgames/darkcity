<?php
/**
 * controleur.php : Controleur de navigation de DarkCity 2
 *
 * Date    : 09/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribu� selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */  

	error_reporting('E_ERROR');

	include_once("include/constants.php");
	include_once("include/Config.class.php");
	include_once("include/LogManager.class.php");
	include_once("include/DateManager.class.php");
	include_once("include/ErrorManager.class.php");
	include_once("dao/Datasource.php");

	$config = new Config();
	$logManager = new LogManager("log/darkcity2.log");

	$logManager->message("controlleur : Connection a base de donnees");
	$datasource = new Datasource($config->getDatabaseHost(), $config->getDatabaseName(), $config->getDatabaseUser(), $config->getDatabasePassword());
	
	$dateManager = new DateManager();
	$errorManager = new ErrorManager();
	
	// La connection
	if ($_GET["action"] == "connecter") {
		include("view/notfound.view.php");
	}
	else if ($_GET["action"] == "nouveau") {
		include("view/notfound.view.php");
	}
	else if ($_GET["action"] == "oubli") {
		include("view/notfound.view.php");
	}
	// Les pages du site
	else if ($_GET["action"] == "highscores") {
		include("view/notfound.view.php");
	}
	else if ($_GET["action"] == "screenshots") {
		include("view/screenshots.view.php");
	}
	else if ($_GET["action"] == "remerciements") {
		include("view/credits.view.php");
	}
	else if ($_GET["action"] == "contacts") {
		include("view/contact.view.php");
	}
	else if ($_GET["action"] == "soutien") {
		include("view/soutiens.view.php");
	}
	else if ($_GET["action"] == "news") {
		include("model/init_login.model.php");
		include("view/news.view.php");
	}
	else {
		// Page de login
		$logManager->message("controlleur : pas de user => redirection vers login");
		include("model/init_login.model.php");
		include("view/login.view.php");
	}
	exit;
?>
