<?php
/**
 * DiceManagerTest.php : Test PHP Unit du manager de D�s
 *
 * Date    : 11/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribu� selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */

 class DiceManagerTest extends PHPUnit_Framework_TestCase {

	/**
	 * roll - lancer des des
	 */ 
	function testRoll() {
		include_once("../include/DiceManager.class.php");
		$dice = new DiceManager();
		$c = false;
		$v0 = 0;
		for ($i = 0; $i<100; $i++) {
			$v = $dice->roll(100, 0);
			$this->assertTrue(($v >= 0 and $v <= 99));
			if ($i > 1) {
				if ($v != $v0) $c = true;
			}
			$v0=$v;
		}
		$this->assertTrue($c);
	}
   
 }
?>
