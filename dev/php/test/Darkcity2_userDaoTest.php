<?php
/**
 * Darkcity2_xptableDao.php : Test PHPUnit du DAO de Darkcity2_xptable
 *
 * Date    : 09/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribué selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */
class Darkcity2_xptableTest extends PHPUnit_Framework_TestCase {

	function testSessionWrite() {
		include_once("../include/Config.class.php");
		include_once("../dao/Datasource.php");
		include_once("../dao/object/Darkcity2_user.php");
		include_once("../dao/accessor/Darkcity2_userDao.php");
		$cfg = new Config();
		$ds = new Datasource($cfg->getDatabaseHost(), $cfg->getDatabaseName(), $cfg->getDatabaseUser(), $cfg->getDatabasePassword());
		$userDao = new Darkcity2_userDao();
		
		$user = $userDao->createValueObject();
		$user->setUser_email("pbousquet33@gmail.com");
		$user->setUser_password("12345");
		$user->setUser_nom("Bousquet");
		$user->setUser_prenom("Philippe");
		$user->setUser_actif(1);
		$user->setUser_bloque(0);
		$user->setUser_dnaiss("1976-12-28");
		$userDao->create(&$ds, &$user);
		
		$user2 = $userDao->getObject(&$ds, "pbousquet33@gmail.com");
		$userDao->delete(&$ds, $user);

		$this->assertEquals($user->getUser_email(), $user2->getUser_email());
		$this->assertEquals($user->getUser_nom(), $user2->getUser_nom());
		$this->assertEquals($user->getUser_prenom(), $user2->getUser_prenom());
		$this->assertEquals($user->getUser_dnaiss(), $user2->getUser_dnaiss());
		$this->assertEquals($user->getUser_password(), $user2->getUser_password());
		$this->assertEquals($user->getUser_actif(), $user2->getUser_actif());
		$this->assertEquals($user->getUser_bloque(), $user2->getUser_bloque());
	}
	
}
?>
