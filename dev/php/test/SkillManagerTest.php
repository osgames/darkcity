<?php
/**
 * SkillManagerTest.php : Test PHP Unit du manager de Comp�tence
 *
 * Date    : 11/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribu� selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */

 class SkillManagerTest extends PHPUnit_Framework_TestCase {

	function testTableCarac() {
		include_once("../include/DiceManager.class.php");
		include_once("../include/SkillManager.class.php");
		$skillManager = new SkillManager();
		foreach ($skillManager->caracs as $k=>$v) {
			$this->assertTrue(isset($v['TITLE']));
			$this->assertTrue(isset($v['DESC']));
		}
		
	}

	function testTableSkill() {
		include_once("../include/DiceManager.class.php");
		include_once("../include/SkillManager.class.php");
		$skillManager = new SkillManager();
		foreach ($skillManager->skills as $k=>$v) {
			$this->assertTrue(isset($v['TITLE']));
			$this->assertTrue(isset($v['DESC']));
			$this->assertTrue(isset($v['CARAC']));
			$total_cmp = 0;
			foreach ($v['CARAC'] as $k1=>$v1) {
				$total_cmp+=$v1;
			}
			$this->assertEquals($k." "."100", $k." ".$total_cmp);
		}
	}
   
 }
?>
