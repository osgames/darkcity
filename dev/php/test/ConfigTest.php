<?php
/**
 * ConfigTest.php : Test PHPUnit de la config
 *
 * Date    : 09/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribué selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */
class ConfigTest extends PHPUnit_Framework_TestCase {

	function testConfig() {
		include_once("../include/Config.class.php");
		$cfg = new Config();
		$this->assertEquals('darkcity2', $cfg->getDatabaseUser());
		$this->assertEquals('motpasse', $cfg->getDatabasePassword());
		$this->assertEquals('127.0.0.1', $cfg->getDatabaseHost());
		$this->assertEquals('darkcity2', $cfg->getDatabaseName());
		$this->assertEquals('2.0.0', $cfg->getGameVersion());
	}
	
}
?>
