<?php
/**
 * ErrorManagerTest.php : Test PHPUnit de la gestion des erreurs
 *
 * Date    : 14/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribué selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */
class ErrorManagerTest extends PHPUnit_Framework_TestCase {

	function testErreur() {
		include_once("../include/ErrorManager.class.php");
		$errorManager = new ErrorManager();
		$this->assertFalse($errorManager->isError());
		$errorManager->setError("Erreur 1");
		$this->assertTrue($errorManager->isError());
		$this->assertEquals('Erreur 1', $errorManager->getError());
	}
	
}
?>
