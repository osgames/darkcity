<?php
/**
 * Darkcity2_xptableDao.php : Test PHPUnit du DAO de Darkcity2_xptable
 *
 * Date    : 09/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribué selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */
class Darkcity2_sessionDaoTest extends PHPUnit_Framework_TestCase {

	function testSessionWrite() {
		include_once("../include/Config.class.php");
		include_once("../dao/Datasource.php");
		include_once("../dao/object/Darkcity2_session.php");
		include_once("../dao/accessor/Darkcity2_sessionDao.php");
		$cfg = new Config();
		$ds = new Datasource($cfg->getDatabaseHost(), $cfg->getDatabaseName(), $cfg->getDatabaseUser(), $cfg->getDatabasePassword());
		$sessionDao = new Darkcity2_sessionDao();
		
		$session = $sessionDao->createValueObject();
		$session->setSession_user("darken33@free.fr");
		$session->setSession_sessionid("PHP12345");
		$session->setSession_lastaction(123445);
		$sessionDao->create(&$ds, &$session);
		
		$session2 = $sessionDao->getObject(&$ds, "darken33@free.fr");
		$this->assertEquals($session->getSession_user(), $session2->getSession_user());
		$this->assertEquals($session->getSession_sessionid(), $session2->getSession_sessionid());
		$this->assertEquals($session->getSession_lastaction(), $session2->getSession_lastaction());

		$sessionDao->delete(&$ds, $session);
	}
	
}
?>
