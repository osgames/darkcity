<?php
/**
 * Darkcity2_xptableDao.php : Test PHPUnit du DAO de Darkcity2_xptable
 *
 * Date    : 09/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribué selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */
class Darkcity2_xptableTest extends PHPUnit_Framework_TestCase {

	function testXptableRead() {
		include_once("../include/Config.class.php");
		include_once("../dao/Datasource.php");
		include_once("../dao/object/Darkcity2_xptable.php");
		include_once("../dao/accessor/Darkcity2_xptableDao.php");
		$cfg = new Config();
		$ds = new Datasource($cfg->getDatabaseHost(), $cfg->getDatabaseName(), $cfg->getDatabaseUser(), $cfg->getDatabasePassword());
		$xptableDao = new Darkcity2_xptableDao();
		$level = $xptableDao->getObject(&$ds, "1");
		$this->assertEquals('1', $level->getXptable_level());
		$this->assertEquals('0', $level->getXptable_xp());
		$level2 = $xptableDao->getObject(&$ds, "99");
		$this->assertEquals('99', $level2->getXptable_level());
		$this->assertEquals('13034431', $level2->getXptable_xp());
		$sql = "SELECT * FROM darkcity2_xptable WHERE xptable_xp <= '13000000' ORDER BY xptable_level DESC;";
		$levelList = $xptableDao->listQuery(&$ds, $sql); 
 		$this->assertEquals('98', $levelList[0]->getXptable_level());
	}
	
}
?>
