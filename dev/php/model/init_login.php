 <?php 
/**
 * init_login.php : partie m�tier de la page login
 *
 * Date    : 14/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribu� selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */    

  
  $logManager->message("init_login : debut");
  
  include_once("dao/accessor/Darkcity2_sessionDao.php");
  include_once("dao/object/Darkcity2_session.php");
    
  // On r�cup�re toutes les session ouverte (Nb user conncet�)
  $sessionDao = new Darkcity2_sessionDao;
  $date = $dateManager->getPastDate(30*$MINUTE);
  $sql = "SELECT * FROM darkcity2_session WHERE session_lastaction >= ".$date.";" ;
  $sessionlist = $sessionDao->listQuery(&$datasource, $sql);
  $nbConnect = sizeof($sessionlist));

?>
