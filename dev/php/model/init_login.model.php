 <?php 
/**
 * init_login.model.php : partie m�tier de la page login
 *
 * Date    : 14/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribu� selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */    

  
  $logManager->message("init_login.model : debut");
 
  include_once("dao/accessor/Darkcity2_sessionDao.php");
  include_once("dao/object/Darkcity2_session.php");
  include_once("dao/accessor/Darkcity2_userDao.php");
  include_once("dao/object/Darkcity2_user.php");
  include_once("dao/object/Darkcity2_news.php");
  include_once("dao/accessor/Darkcity2_newsDao.php");
  include_once("bean/NewsDataBean.php");
    
  // On r�cup�re toutes les session ouverte (Nb user conncet�)
  $logManager->message("init_login.model : nombre d'utilisateurs");
  $sessionDao = new Darkcity2_sessionDao();
  $userDao = new Darkcity2_userDao();
 
  $date = $dateManager->getPastDate(30*$MINUTE);
  $sql = "SELECT * FROM darkcity2_session WHERE session_lastaction >= ".$date.";" ;

  $sessionlist = $sessionDao->listQuery(&$datasource, $sql);
  $nbConnect = sizeof($sessionlist);
  $nbUser = $userDao->countAll(&$datasource);
 
  $logManager->message("init_login.model : les news");
  $newsDao = new Darkcity2_newsDao();
  $newsDataBean = new NewsDataBean();
  $sql = "SELECT * FROM darkcity2_news WHERE news_visible = '".$TRUE."' ORDER BY news_creation DESC;";
  $newsList = $newsDao->listQuery(&$datasource, $sql); 
  $newsDataBean->setNews($newsList);
  	
  $logManager->message("init_login.model : fin");
?>
