<?php 
/**
 * soutiens.php : vue correspondant à la page de soutien
 *
 * Date    : 18/03/2009
 * Auteur  : Philippe Bousquet 
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2008-2009 Philippe Bousquet.
 * 
 * Ce logiciel est distribué selon le termes de la GNU General Public License v2.
 */

 $logManager->message("page soutiens : debut"); 
 include_once("view/header_login.php"); 
?>
				<div class="main">
					<p/>
					<div class="box" style="text-align: center; padding: 5px; border: #CFCFCF 1px solid; background: #652020; -moz-border-radius-topleft: 8px; -webkit-border-top-left-radius: 8px; border-top-left-radius: 8px;  -moz-border-radius-bottomleft: 8px; -webkit-border-bottom-left-radius: 8px; border-bottom-left-radius: 8px; -moz-border-radius-topright: 8px; -webkit-border-top-right-radius: 8px; border-top-right-radius: 8px;  -moz-border-radius-bottomright: 8px; -webkit-border-bottom-right-radius: 8px; border-bottom-right-radius: 8px;">
						<h3>Nous soutenir</h3>
						<div class="scenario-box" style="text-align: left;">
							<p/>
							<strong>DarkCity 2</strong> est un jeu gratuit, et il le restera !!!<br/>
							<p/>
							Nous avons cependant besoin de votre soutien, et vous pouvez le faire de plusieurs manières différentes :<br/>
							<p/>
							<strong>1. Nous envoyer un petit email</strong><br/>
							Recevoir un retour sur ce que l'on fait est toujours gratifiant, donc si le coeur vous en dit, n'hésitez pas à nous envoyer un email 
							pour nous faire part de vos remarques.<br/>
							Pour cela il suffit d'envoyer un email à <a href="mailto:darkcitygame@free.fr">darkcitygame@free.fr</a>
							<p/>
							<strong>2. Faire un don</strong><br/>
							Ces dons pourront servir à améliorer encore plus et plus rapidement le service et le jeu en lui meme, 
							la première chose à quoi serviront vos dons sera le financement d'un serveur afin de proposer un service 
							plus stable et plus robuste qu'actuellement.<br/>
							Pour cela il suffit de cliquer sur le bouton ci-dessous :<br/>
							<div style="text-align: center;">
								<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
									<input type="hidden" name="cmd" value="_s-xclick" />
									<input type="hidden" name="hosted_button_id" value="2850810" />
									<input type="image" src="https://www.paypal.com/fr_FR/FR/i/btn/btn_donate_LG.gif" name="submit" alt="" />
									<img alt="" border="0" src="https://www.paypal.com/fr_FR/i/scr/pixel.gif" width="1" height="1" />
								</form>
							</div>  
							<p/>
							<strong>3. Faire connaitre le jeu</strong><br/>
							Le plus important de tout étant de parler de ce jeu autour de vous, car plus il y aura de joueurs et plus le jeu sera interressant. En effet la grosse partie du jeu est le PvP.
							Alors si ce jeu vous plait, n'hésitez pas à en parler autour de vous. 
							<p/>
							Sachez que n'importe laquelle des solutions que vous choisirez pour manifester votre soutien, nous ira droit au coeur.
							<br/>
							<br/>
						</div>
					</div>  
				</div>
			</div>
		</div>
		<hr />
<?php      
  include("view/footer_login.php");
  $logManager->message("page soutiens : fin"); 
?>
