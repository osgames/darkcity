<?php 
/**
 * screenshots.php : vue correspondant � la page des images du jeu
 *
 * Date    : 18/03/2009
 * Auteur  : Philippe Bousquet 
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2008-2009 Philippe Bousquet.
 * 
 * Ce logiciel est distribu� selon le termes de la GNU General Public License v2.
 */

 $logManager->message("page screenshots : debut"); 
  include("view/header_login.php");
?>
				<div class="main">
					<p/>
					<div class="box" style="text-align: center; padding: 5px; border: #CFCFCF 1px solid; background: #652020; -moz-border-radius-topleft: 8px; -webkit-border-top-left-radius: 8px; border-top-left-radius: 8px;  -moz-border-radius-bottomleft: 8px; -webkit-border-bottom-left-radius: 8px; border-bottom-left-radius: 8px; -moz-border-radius-topright: 8px; -webkit-border-top-right-radius: 8px; border-top-right-radius: 8px;  -moz-border-radius-bottomright: 8px; -webkit-border-bottom-right-radius: 8px; border-bottom-right-radius: 8px;">
						<h3>DarkCity 2 : La Page d'accueil du site</h3>
						<div class="scenario-box" style="text-align: center;">
							<img src="images/screenshots/accueil.png" style="width: 600px; height: 353px;	" alt="" title="" />
						</div>
					</div>  

					<div class="box" style="text-align: center; padding: 5px; border: #CFCFCF 1px solid; background: #652020; -moz-border-radius-topleft: 8px; -webkit-border-top-left-radius: 8px; border-top-left-radius: 8px;  -moz-border-radius-bottomleft: 8px; -webkit-border-bottom-left-radius: 8px; border-bottom-left-radius: 8px; -moz-border-radius-topright: 8px; -webkit-border-top-right-radius: 8px; border-top-right-radius: 8px;  -moz-border-radius-bottomright: 8px; -webkit-border-bottom-right-radius: 8px; border-bottom-right-radius: 8px;">
						<h3>DarkCity 1 : Les Screenshots de l'ancienne version du jeu</h3>
						<div class="scenario-box" style="text-align: center;">
							<img src="images/screenshots/DarkCity_capture01.png" style="width: 200px; height: 117px;" alt="" title="" />
							<img src="images/screenshots/DarkCity_capture02.png" style="width: 200px; height: 117px;" alt="" title="" />
							<img src="images/screenshots/DarkCity_capture03.png" style="width: 200px; height: 117px;" alt="" title="" />
							<br/>
							<img src="images/screenshots/DarkCity_capture04.png" style="width: 200px; height: 117px;" alt="" title="" />
							<img src="images/screenshots/DarkCity_capture05.png" style="width: 200px; height: 117px;" alt="" title="" />
							<img src="images/screenshots/DarkCity_capture06.png" style="width: 200px; height: 117px;" alt="" title="" />
							<br/>
							<img src="images/screenshots/DarkCity_capture07.png" style="width: 200px; height: 117px;" alt="" title="" />
							<img src="images/screenshots/DarkCity_capture08.png" style="width: 200px; height: 117px;" alt="" title="" />
						</div>
					</div>  
				</div>
			</div>
		</div>
<?php      
  include("view/footer_login.php");
  $logManager->message("screenshots.view : debut"); 
?>
