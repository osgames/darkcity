<?php 
/**
 * news.php : vue correspondant � la page des news du jeu
 *
 * Date    : 18/03/2009
 * Auteur  : Philippe Bousquet 
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2008-2009 Philippe Bousquet.
 * 
 * Ce logiciel est distribu� selon le termes de la GNU General Public License v2.
 */

 $logManager->message("page news : debut"); 
 require("includes/langage.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>DarkCity - MMORPG par Navigateur</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <meta http-equiv="keywords" content="darkcity, mmorpg, online, free, libre, jeu" />
    <link rel="stylesheet" href="themes/default/style.css" type="text/css" />
  </head>
  <body>
  <div class="border">
    <div class="body">
<?php
  echo "      <form action=\"index.php\" method=\"get\">\n";   
?>
        <div class="title"><img class="title" src="images/jeu/ville2.jpg" alt="DARKCITY - La ville est &agrave; toi" title=""/></div>
        <hr />
        <div class="menu">
          <input type="hidden" name="pageCourante" value="news" />
          <input name="action" type="submit" class="menu" value="accueil"/> 
          <input name="action" type="submit" class="menu" value="news"/> 
          <input name="action" type="submit" class="menu" value="images"/> 
          <input name="action" type="submit" class="menu" value="cr&eacute;dits"/>
          <input name="action" type="submit" class="menu" value="soutien"/>
          <input name="action" type="submit" class="menu" value="contacts"/>
        </div>
      </form>
      <hr />

      
      <div class="main">
<?php
  if ($errorManager->isError()) {
    echo "      <div class=\"error\">".$errorManager->getMessage()."</div>\n";
  }
?>
        <p/>
        <div class="box">
          <div class="titlebox">Les derni�res nouvelles de DarkCity</div>
<?php 
  if ($newsDataBean->getNbNews() == 0) {
    echo "          Pas de nouvelles, bonnes nouvelles...";
  }
?>
        </div>  
        
<?php
  for ($i = 0; $i < $newsDataBean->getNbNews(); $i++) {
    $news = $newsDataBean->getNewsAt($i);
    echo '          <div class="box">'."\n";
    echo '            <div class="titlebox_g">'.$news->getNews_titre().'</div>'."\n";
    echo '            <div class="text-box" style="text-align: left;">'."\n";
    echo '          '.translate($news->getNews_texte())."\n";
    echo '            </div>'."\n";
    echo '            <div class="actions-right">'."\n";
    echo '              &nbsp;'."\n";
    echo '            </div>'."\n";
    echo '          </div>'."\n";
  }
?> 
      </div>
      <hr />
<?php 
  include("vue/footer.php");
  $logManager->message("Page g�n�r�e en ".$chrono->stop()."s.");  
  $logManager->message("page news : fin"); 
?>