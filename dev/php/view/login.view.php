<?php 
/**
 * login.view.php : vue correspondant � la page d'authentification
 *
 * Date    : 14/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribu� selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */
 
  $logManager->message("login.view : debut"); 
  require("include/langage.inc.php");
  include("view/header_login.php");
?>
				
				<div class="main">
<?php
  if ($errorManager->isError()) {
?>	  
					<div class="error"><?=$errorManager->getMessage();?></div>
<?php
  }
?>  
					<div class="box" style="text-align: center; padding: 5px; border: #CFCFCF 1px solid; background: #652020; -moz-border-radius-topleft: 8px; -webkit-border-top-left-radius: 8px; border-top-left-radius: 8px;  -moz-border-radius-bottomleft: 8px; -webkit-border-bottom-left-radius: 8px; border-bottom-left-radius: 8px; -moz-border-radius-topright: 8px; -webkit-border-top-right-radius: 8px; border-top-right-radius: 8px;  -moz-border-radius-bottomright: 8px; -webkit-border-bottom-right-radius: 8px; border-bottom-right-radius: 8px;">
						Il y actuellement <?=$nbConnect;?> joueurs en ligne sur un total de <?=$nbUser;?>.
					</div>

					<div class="box" style="text-align: center; padding: 5px; border: #CFCFCF 1px solid; background: #652020; -moz-border-radius-topleft: 8px; -webkit-border-top-left-radius: 8px; border-top-left-radius: 8px;  -moz-border-radius-bottomleft: 8px; -webkit-border-bottom-left-radius: 8px; border-bottom-left-radius: 8px; -moz-border-radius-topright: 8px; -webkit-border-top-right-radius: 8px; border-top-right-radius: 8px;  -moz-border-radius-bottomright: 8px; -webkit-border-bottom-right-radius: 8px; border-bottom-right-radius: 8px;">
						<img class="image" src="images/armes/Arasaka_253.gif" alt="" title="" />
						<div class="text-box" style="text-align: left">
							<strong>DarkCity</strong> est une ville des ann&eacute;es 2020, o&ugrave; la criminalit&eacute; est un vrai probl&egrave;me, les forces de police sont d&eacute;bord&eacute;es et donc inefficaces, elles d&eacute;cident ainsi de faire appel &agrave; des chasseurs de primes comme dans le Far West de l'&eacute;poque afin de faire baisser la criminalit&eacute;. 
							De l'autre cot&eacute;, la mafia s'organise et lance &agrave; son tour des contrats sur les t&ecirc;tes de ces nouveaux justiciers.
							<p/>
							Vous, vous d&eacute;barquez juste dans cette ville chaotique, &agrave; vous de choisir quel cot&eacute; vous allez servir. La Justice ou la Criminalit&eacute;...
							<p/>
							Pour obtenir de l'aide sur le jeu ou avoir de plus amples informations sur celui-ci, veuillez visiter le site : <a href="http://darken33.free.fr/index.php?cat=8&amp;rub=35">http://darken33.free.fr/</a>,<br/> 
							Vous pouvez &eacute;galment nous contacter : <a href="mailto:darkcitygame@free.fr">darkcitygame@free.fr</a><br/><br/>
						</div>
					</div>

<?php 
  if ($newsDataBean->getNbNews() == 0) {
?>	  
					<div class="box" style="text-align: center; padding: 5px; border: #CFCFCF 1px solid; background: #652020; -moz-border-radius-topleft: 8px; -webkit-border-top-left-radius: 8px; border-top-left-radius: 8px;  -moz-border-radius-bottomleft: 8px; -webkit-border-bottom-left-radius: 8px; border-bottom-left-radius: 8px; -moz-border-radius-topright: 8px; -webkit-border-top-right-radius: 8px; border-top-right-radius: 8px;  -moz-border-radius-bottomright: 8px; -webkit-border-bottom-right-radius: 8px; border-bottom-right-radius: 8px;">
						<div class="text-box" style="text-align: left">
							Pas de nouvelles, bonnes nouvelles...
						</div>        
					</div>        
<?
  }
  $nb = ($newsDataBean->getNbNews() < 5 ? $newsDataBean->getNbNews() : 5);
  for ($i = 0; $i < $nb; $i++) {
    $news = $newsDataBean->getNewsAt($i);
?>
					<div class="box" style="text-align: center; padding: 5px; border: #CFCFCF 1px solid; background: #652020; -moz-border-radius-topleft: 8px; -webkit-border-top-left-radius: 8px; border-top-left-radius: 8px;  -moz-border-radius-bottomleft: 8px; -webkit-border-bottom-left-radius: 8px; border-bottom-left-radius: 8px; -moz-border-radius-topright: 8px; -webkit-border-top-right-radius: 8px; border-top-right-radius: 8px;  -moz-border-radius-bottomright: 8px; -webkit-border-bottom-right-radius: 8px; border-bottom-right-radius: 8px;">
						<div class="text-box" style="text-align: left">
							<h3><?=$news->getNews_creation()." - ".$news->getNews_titre();?></h3>
							<?=translate($news->getNews_texte());?>
						</div>        
					</div>        
<?
  }
  if ($newsDataBean->getNbNews() > 5) {
?>
					<div style="text-align: right; margin-top: 25px; margin-right: 25px;">
						<a href="index.php?action=news"/>Voir l'ensemble des nouvelles</a> 
					</div>	
<?
  }
?>  					
				</div>
			</div>				
		</div>  
<?php 
  include("view/footer_login.php");
  $logManager->message("login.view : debut"); 
?>
