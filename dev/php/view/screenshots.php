<?php 
/**
 * screenshots.php : vue correspondant � la page des images du jeu
 *
 * Date    : 18/03/2009
 * Auteur  : Philippe Bousquet 
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2008-2009 Philippe Bousquet.
 * 
 * Ce logiciel est distribu� selon le termes de la GNU General Public License v2.
 */

 $logManager->message("page screenshots : debut"); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>DarkCity - MMORPG par Navigateur</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <meta http-equiv="keywords" content="darkcity, mmorpg, online, free, libre, jeu" />
    <link rel="stylesheet" href="themes/default/style.css" type="text/css" />
  </head>
  <body>
  <div class="border">
    <div class="body">
      <form action="index.php" method="get"> 
      <div class="title"><img class="title" src="images/jeu/ville2.jpg" alt="DARKCITY - La ville est &agrave; toi" title=""/></div>
      <hr />
      <div class="menu">
        <input type="hidden" name="pageCourante" value="news" />
        <input name="action" type="submit" class="menu" value="accueil"/> 
        <input name="action" type="submit" class="menu" value="news"/> 
        <input name="action" type="submit" class="menu" value="images"/> 
        <input name="action" type="submit" class="menu" value="cr&eacute;dits"/>
        <input name="action" type="submit" class="menu" value="soutien"/>
          <input name="action" type="submit" class="menu" value="contacts"/>
      </div>
      </form>
      <hr />
      <div class="main">
        <p/>
        <div class="box">
          <div class="titlebox">La Fiche de personnage</div>
          <div class="scenario-box" style="text-align: center;">
            <img src="images/divers/screenshots/DarkCity_capture01.png" alt="" title="" />
          </div>
        </div>  
        <div class="box">
          <div class="titlebox">Acheter un v�hicule</div>
          <div class="scenario-box" style="text-align: center;">
            <img src="images/divers/screenshots/DarkCity_capture02.png" alt="" title="" />
          </div>
        </div>  
        <div class="box">
          <div class="titlebox">Le plan de la ville</div>
          <div class="scenario-box" style="text-align: center;">
            <img src="images/divers/screenshots/DarkCity_capture03.png" alt="" title="" />
          </div>
        </div>  
        <div class="box">
          <div class="titlebox">Une petite vir�e dans un bar</div>
          <div class="scenario-box" style="text-align: center;">
            <img src="images/divers/screenshots/DarkCity_capture04.png" alt="" title="" />
          </div>
        </div>  
        <div class="box">
          <div class="titlebox">Qu'il fait bon chez soi</div>
          <div class="scenario-box" style="text-align: center;">
            <img src="images/divers/screenshots/DarkCity_capture05.png" alt="" title="" />
          </div>
        </div>  
        <div class="box">
          <div class="titlebox">Etape oblig�e, l'armurier du coin</div>
          <div class="scenario-box" style="text-align: center;">
            <img src="images/divers/screenshots/DarkCity_capture06.png" alt="" title="" />
          </div>
        </div>  
        <div class="box">
          <div class="titlebox">Un avis de recherche int�ressant</div>
          <div class="scenario-box" style="text-align: center;">
            <img src="images/divers/screenshots/DarkCity_capture07.png" alt="" title="" />
          </div>
        </div>  
        <div class="box">
          <div class="titlebox">Rencontre avec un bandit dans les rues</div>
          <div class="scenario-box" style="text-align: center;">
            <img src="images/divers/screenshots/DarkCity_capture08.png" alt="" title="" />
          </div>
        </div>  
      </div>
      <hr />
<?php      
  include("vue/footer.php");
  $logManager->message("Page g�n�r�e en ".$chrono->stop()."s.");  
  $logManager->message("page screenshots : fin"); 
?>
