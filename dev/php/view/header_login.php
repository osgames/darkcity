<?php 
/**
 * login.view.php : vue correspondant à la page d'authentification
 *
 * Date    : 14/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribué selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */
?>
<!DOCTYPE html>
<html>
	<head>
		<title>DarkCity 2 - La revanche des justiciers</title>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		<meta http-equiv="keywords" content="darkcity, mmorpg, online, free, libre, jeu" />
		<link rel="stylesheet" href="theme/default/style.css" type="text/css" />
	</head>
	<body>
		<div class="border">
			<div class="body">
				<form action="index.php" method="get">   
					<input type="hidden" name="pageCourante" value="login" />
					<div class="title" style="background: url(images/jeu/titreD2.jpg); height: 200px;">
						<img class="title" style="float: left;" src="images/jeu/titrev2.jpg" alt="DARKCITY 2 - La revanche des justiciers" title=""/>
						<div style="padding-top: 30px;">
							<div style="margin-left: 600px; padding: 5px; text-align: right; border-top: #CFCFCF 1px solid; border-bottom: #CFCFCF 1px solid; border-left: #CFCFCF 1px solid; background: #652020;  opacity: .7; filter:Alpha(Opacity=70); -moz-border-radius-topleft: 8px; -webkit-border-top-left-radius: 8px; border-top-left-radius: 8px;  -moz-border-radius-bottomleft: 8px; -webkit-border-bottom-left-radius: 8px; border-bottom-left-radius: 8px;">
								<div style="padding-top: 3px; padding-bottom: 3px;">
									<label for="email" style="font-weight: bold; width: 150px">E-mail : </label>
									<input id="email" name="email" class="text" style="width: 125px; background: none; border: none; border-bottom: #CFCFCF 1px solid;" type="text" maxlength="25" value="<?=(isset($nom)?$nom:"")?>"/> 
									&nbsp;<input name="action" class="action" style="padding-top: 0px;" type="submit" value="connecter" title="Connectez vous..."/>
								</div>
								<div style="padding-top: 3px; padding-bottom: 3px;">
									<label for="login" style="font-weight: bold;">Password : </label>
									<input id="password" name="password" class="text" style="width: 125px; background: none; border: none; border-bottom: #CFCFCF 1px solid;" type="password" maxlength="25" value="<?=(isset($nom)?$nom:"")?>"/> 
									&nbsp;<input name="action" class="action" type="submit" value="nouveau" title="Cr&eacute;ez un nouveau personnage" />
								</div>
								<div style="padding-top: 3px; padding-bottom: 3px; text-align: center;">
									<a href="index.php?action=oubli"/>J'ai oubli&eacute; mon mot de passe</a> 
								</div>
							</div>
						</div>
					</div>
					<div style="clear: both;"></div>
					<hr />
					<div class="menu">
						<input name="action" type="submit" class="menu" value="accueil"/> 
						<input name="action" type="submit" class="menu" value="highscores"/> 
						<input name="action" type="submit" class="menu" value="screenshots"/> 
						<input name="action" type="submit" class="menu" value="remerciements"/>
						<input name="action" type="submit" class="menu" value="soutien"/>
						<input name="action" type="submit" class="menu" value="contacts"/>
					</div>
				</form>
				<hr />
