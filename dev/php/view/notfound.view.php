<?php 
/**
 * credits.php : vue correspondant à la page des crédits & remerciments
 *
 * Date    : 18/03/2009
 * Auteur  : Philippe Bousquet 
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2008-2009 Philippe Bousquet.
 * 
 * Ce logiciel est distribué selon le termes de la GNU General Public License v2.
 */

 $logManager->message("page credits : debut"); 
 include_once("view/header_login.php"); 
?>
				<div class="main">
					<p/>
					<div class="box" style="text-align: center; padding: 5px; border: #CFCFCF 1px solid; background: #652020; -moz-border-radius-topleft: 8px; -webkit-border-top-left-radius: 8px; border-top-left-radius: 8px;  -moz-border-radius-bottomleft: 8px; -webkit-border-bottom-left-radius: 8px; border-bottom-left-radius: 8px; -moz-border-radius-topright: 8px; -webkit-border-top-right-radius: 8px; border-top-right-radius: 8px;  -moz-border-radius-bottomright: 8px; -webkit-border-bottom-right-radius: 8px; border-bottom-right-radius: 8px;">
						<h3>Fonctionalité non encore diponible</h3>
						<div class="action-box" style="text-align: left;">
							<p/>
							Aujourd'hui seul le site consacré à <strong>DarkCity</strong> a été réactivé<br/>
							<p/>
							Le jeu est pour l'instant en cours de développement et devrait apparitre dans les prochains mois.<br/>
							Encore merci de votre patience...
							<p/>
						</div>
					</div>  
				</div>
			</div>
      </div>
      <hr />
<?php 
  include("view/footer_login.php");
  $logManager->message("page credits : fin"); 
?>
