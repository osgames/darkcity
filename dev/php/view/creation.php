<?php 
/**
 * creation.php : vue correspondant � la page de creation de personnage
 *
 * Date    : 21/03/2009
 * Auteur  : Philippe Bousquet 
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2008-2009 Philippe Bousquet.
 * 
 * Ce logiciel est distribu� selon le termes de la GNU General Public License v2.
 */

 $logManager->message("page creation : debut"); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>DarkCity - MMORPG par Navigateur</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <meta http-equiv="keywords" content="dark, bay, bar, australien, bordeaux, victoire" />
    <link rel="stylesheet" href="themes/default/style.css" type="text/css" />
  </head>
  <body>
  <div class="border">
    <div class="body">
    <?php
      echo "<form action=\"".$sessionManager->parseURL("index.php", "")."\" method=\"post\">";   
    ?>  
        <div class="title"><img class="title" src="images/jeu/ville2.jpg" alt="DARKCITY - La ville est &agrave; toi" title=""/></div>
        <hr />
        <div class="menu">
          <input type="hidden" name="pageCourante" value="news" />
          <input name="action" type="submit" class="menu" value="accueil"/> 
          <input name="action" type="submit" class="menu" value="news"/> 
          <input name="action" type="submit" class="menu" value="images"/> 
          <input name="action" type="submit" class="menu" value="cr&eacute;dits"/>
          <input name="action" type="submit" class="menu" value="soutien"/>
          <input name="action" type="submit" class="menu" value="contacts"/>
        </div>
    </form>
      <hr />
    <?php
      echo "<form action=\"".$sessionManager->parseURL("index.php", "")."\" method=\"post\">";   
    ?>  
      <div class="main">
        <input type="hidden" name="pageCourante" value="creation" />
        <?php
          if ($errorManager->isError()) {
            echo "<div class=\"error\">";
            echo $errorManager->getMessage();
            echo "</div>";
          }
        ?>
        <div class="box" style="width:550px; margin-left:125px; margin-bottom:100px;">
          <div class="titlebox">Cr�ation d'un nouveau personnage</div>
          <div class="text-box">
            <?php echo "<input type=\"hidden\" name=\"avertissement\" value=\"".(isset($avertissement)?$avertissement:"")."\" />"; ?>
            <table class="login" cellspacing="5" cellpadding="5">
              <tr><th class="login">Pseudonyme</th><td style="text-align: left"><?php echo "<input name=\"nom\" class=\"text\" style=\"width: 100px;\" type=\"text\" maxlength=\"25\" value=\"".(isset($nom)?$nom:"")."\"/>"; ?></td></tr>
              <tr><th class="login">Mot de passe</th><td style="text-align: left"><input name="password" class="text" style="width: 200px;" type="password" maxlength="25" value=""/></td></tr>
              <tr><th class="login">Confirmation</th><td style="text-align: left"><input name="password2" class="text" style="width: 200px;" type="password" maxlength="25" value=""/></td></tr>
              <tr><th class="login">Email joueur</th><td style="text-align: left"><?php echo "<input name=\"email\" class=\"text\" style=\"width: 300px;\" type=\"text\" maxlength=\"25\" value=\"".(isset($email)?$email:"")."\"/>"; ?></td></tr>
            </table>  
            <div style="text-align: left; padding-left: 12px;"><strong>Choisissez un avatar</strong><br/>
          <?php
            $av = (isset($avatar) ? $avatar : "");
          ?>
            <table style="border: red 1px solid;">
              <tr>
                <td><img class="mini-photo" src="images/personnes/girl001.gif" alt="girl001" /></td>
                <td><img class="mini-photo" src="images/personnes/girl002.gif" alt="girl002" /></td>
                <td><img class="mini-photo" src="images/personnes/girl003.gif" alt="girl003" /></td>
                <td><img class="mini-photo" src="images/personnes/girl004.gif" alt="girl004" /></td>
                <td><img class="mini-photo" src="images/personnes/girl005.gif" alt="girl005" /></td>
              </tr>
              <tr style="border-bottom: red 1px solid;">
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl001.gif\" ".($av == "personnes/girl001.gif" || $av == "" ? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl002.gif\" ".($av == "personnes/girl002.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl003.gif\" ".($av == "personnes/girl003.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl004.gif\" ".($av == "personnes/girl004.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl005.gif\" ".($av == "personnes/girl005.gif"? "checked=\"checked\"" : "")." />"; ?></td>
              </tr>
              <tr>
                <td><img class="mini-photo" src="images/personnes/man001.gif" alt="man001" /></td>
                <td><img class="mini-photo" src="images/personnes/man002.gif" alt="man002" /></td>
                <td><img class="mini-photo" src="images/personnes/man003.gif" alt="man003" /></td>
                <td><img class="mini-photo" src="images/personnes/man004.gif" alt="man004" /></td>
                <td><img class="mini-photo" src="images/personnes/man005.gif" alt="man005" /></td>
              </tr>
              <tr style="border-bottom: red 1px solid;">
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man001.gif\" ".($av == "personnes/man001.gif" || $av == "" ? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man002.gif\" ".($av == "personnes/man002.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man003.gif\" ".($av == "personnes/man003.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man004.gif\" ".($av == "personnes/man004.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man005.gif\" ".($av == "personnes/man005.gif"? "checked=\"checked\"" : "")." />"; ?></td>
              </tr>
              <tr>
                <td><img class="mini-photo" src="images/personnes/girl006.gif" alt="girl006" /></td>
                <td><img class="mini-photo" src="images/personnes/girl007.gif" alt="girl007" /></td>
                <td><img class="mini-photo" src="images/personnes/girl008.gif" alt="girl008" /></td>
                <td><img class="mini-photo" src="images/personnes/girl009.gif" alt="girl009" /></td>
                <td><img class="mini-photo" src="images/personnes/girl010.gif" alt="girl010" /></td>
              </tr>
              <tr style="border-bottom: red 1px solid;">
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl006.gif\" ".($av == "personnes/girl006.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl007.gif\" ".($av == "personnes/girl007.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl008.gif\" ".($av == "personnes/girl008.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl009.gif\" ".($av == "personnes/girl009.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl010.gif\" ".($av == "personnes/girl010.gif"? "checked=\"checked\"" : "")." />"; ?></td>
              </tr>
              <tr>
                <td><img class="mini-photo" src="images/personnes/man006.gif" alt="man006" /></td>
                <td><img class="mini-photo" src="images/personnes/man007.gif" alt="man007" /></td>
                <td><img class="mini-photo" src="images/personnes/man008.gif" alt="man008" /></td>
                <td><img class="mini-photo" src="images/personnes/man009.gif" alt="man009" /></td>
                <td><img class="mini-photo" src="images/personnes/man010.gif" alt="man010" /></td>
              </tr>
              <tr style="border-bottom: red 1px solid;">
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man006.gif\" ".($av == "personnes/man006.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man007.gif\" ".($av == "personnes/man007.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man008.gif\" ".($av == "personnes/man008.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man009.gif\" ".($av == "personnes/man009.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man010.gif\" ".($av == "personnes/man010.gif"? "checked=\"checked\"" : "")." />"; ?></td>
              </tr>
              <tr>
                <td><img class="mini-photo" src="images/personnes/girl011.gif" alt="girl011" /></td>
                <td><img class="mini-photo" src="images/personnes/girl012.gif" alt="girl012" /></td>
                <td><img class="mini-photo" src="images/personnes/girl013.gif" alt="girl013" /></td>
                <td><img class="mini-photo" src="images/personnes/girl014.gif" alt="girl014" /></td>
                <td><img class="mini-photo" src="images/personnes/girl015.gif" alt="girl015" /></td>
              </tr>
              <tr style="border-bottom: red 1px solid;">
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl011.gif\" ".($av == "personnes/girl011.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl012.gif\" ".($av == "personnes/girl012.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl013.gif\" ".($av == "personnes/girl013.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl014.gif\" ".($av == "personnes/girl014.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl015.gif\" ".($av == "personnes/girl015.gif"? "checked=\"checked\"" : "")." />"; ?></td>
              </tr>
              <tr>
                <td><img class="mini-photo" src="images/personnes/man011.gif" alt="man011" /></td>
                <td><img class="mini-photo" src="images/personnes/man012.gif" alt="man012" /></td>
                <td><img class="mini-photo" src="images/personnes/man013.gif" alt="man013" /></td>
                <td><img class="mini-photo" src="images/personnes/man014.gif" alt="man014" /></td>
                <td><img class="mini-photo" src="images/personnes/man015.gif" alt="man015" /></td>
              </tr>
              <tr style="border-bottom: red 1px solid;">
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man011.gif\" ".($av == "personnes/man011.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man012.gif\" ".($av == "personnes/man012.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man013.gif\" ".($av == "personnes/man013.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man014.gif\" ".($av == "personnes/man014.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man015.gif\" ".($av == "personnes/man015.gif"? "checked=\"checked\"" : "")." />"; ?></td>
              </tr>
              <tr>
                <td><img class="mini-photo" src="images/personnes/girl016.gif" alt="girl016" /></td>
                <td><img class="mini-photo" src="images/personnes/girl017.gif" alt="girl017" /></td>
                <td><img class="mini-photo" src="images/personnes/girl018.gif" alt="girl018" /></td>
                <td><img class="mini-photo" src="images/personnes/girl019.gif" alt="girl019" /></td>
                <td><img class="mini-photo" src="images/personnes/girl020.gif" alt="girl020" /></td>
              </tr>
              <tr>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl016.gif\" ".($av == "personnes/girl016.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl017.gif\" ".($av == "personnes/girl017.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl018.gif\" ".($av == "personnes/girl018.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl019.gif\" ".($av == "personnes/girl019.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/girl020.gif\" ".($av == "personnes/girl020.gif"? "checked=\"checked\"" : "")." />"; ?></td>
              </tr>
              <tr>
                <td><img class="mini-photo" src="images/personnes/man016.gif" alt="man016" /></td>
                <td><img class="mini-photo" src="images/personnes/man017.gif" alt="man017" /></td>
                <td><img class="mini-photo" src="images/personnes/man018.gif" alt="man018" /></td>
                <td><img class="mini-photo" src="images/personnes/man019.gif" alt="man019" /></td>
                <td><img class="mini-photo" src="images/personnes/man020.gif" alt="man020" /></td>
              </tr>
              <tr>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man016.gif\" ".($av == "personnes/man016.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man017.gif\" ".($av == "personnes/man017.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man018.gif\" ".($av == "personnes/man018.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man019.gif\" ".($av == "personnes/man019.gif"? "checked=\"checked\"" : "")." />"; ?></td>
                <td class="center"><?php echo "<input name=\"avatar\" type=\"radio\" value=\"personnes/man020.gif\" ".($av == "personnes/man020.gif"? "checked=\"checked\"" : "")." />"; ?></td>
              </tr>
            </table>
            </div>
            <div style="text-align: left; padding: 12px;"><strong>Entrez une description</strong><br/>
              <textarea name="description" class="textarea" rows="7" cols="60"><?php echo (isset($description)?$description:""); ?></textarea>
            </div>
         </div>
         <div class="actions-center">
           <input name="action" class="action" type="submit" value="cr�er" title="Cr�ez votre personnage..."/>&nbsp;&nbsp;<input name="action" class="action" type="submit" value="retour" title="Cr&eacute;ez un nouveau personnage" />
         </div>
      </div>
      </div>
      </form>
      <hr />
<?php      
  include("vue/footer.php");
  $logManager->message("Page g�n�r�e en ".$chrono->stop()."s.");  
  $logManager->message("page creation : fin"); 
?>
