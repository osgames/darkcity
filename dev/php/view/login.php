<?php 
/**
 * login.view.php : vue correspondant � la page d'authentification
 *
 * Date    : 14/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribu� selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */
 
  $logManager->message("login.view : debut"); 
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Dark City 2 - La revanche des justiciers</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="keywords" content="darkcity, mmorpg, online, free, libre, jeu" />
    <link rel="stylesheet" href="theme/default/style.css" type="text/css" />
  </head>
  <body>
  <div class="border">
    <div class="body">
<?php
  echo "      <form action=\"index.php\" method=\"get\">\n";   
?>  
        <div class="title"><img class="title" src="images/jeu/ville2.jpg" alt="DARKCITY - La ville est &agrave; toi" title=""/></div>
        <hr />
        <div class="menu">
          <input name="action" type="submit" class="menu" value="accueil"/> 
          <input name="action" type="submit" class="menu" value="news"/> 
          <input name="action" type="submit" class="menu" value="images"/> 
          <input name="action" type="submit" class="menu" value="cr&eacute;dits"/>
          <input name="action" type="submit" class="menu" value="soutien"/>
          <input name="action" type="submit" class="menu" value="contacts"/>
        </div>
      </form>
      <hr />

<?php
  echo '      <form action="index.php" method="post">'."\n";   
?>  
        <div class="main">
          <input type="hidden" name="pageCourante" value="login" />
<?php
  if ($errorManager->isError()) {
    echo "          <div class=\"error\">".$errorManager->getMessage()."</div>\n";
  }
  else if ($infoManager->isInfo()) {
    echo '          <div class="box" >'."\n";
    echo '            <table style="width: 700px">'."\n";
    echo "              <tr><th class=\"titlebox\">Messages d'information</th></tr>"."\n";
    echo '              <tr class="tab"><td class="tab">'.$infoManager->getMessage().'</td></tr>'."\n";
    echo '            </table>'."\n";  
    echo '          </div>'."\n";
  }
            echo ' <div class="box" >'."\n";
            echo '  <table style="width: 700px">';
            echo "    <tr><th class=\"titlebox\">Messages d'information</th></tr>";
            echo '    <tr class="tab"><td class="tab">';
            echo 'Il y actuellement '.$nbConnect.' joueurs en ligne.';
            echo '    </td></tr>';
            echo '  </table>';  
            echo '</div>';
?>

          <div class="box">
            <div class="titlebox">Accueil</div>
            <img class="image" src="images/armes/Arasaka_253.gif" alt="" title="" />
            <div class="text-box" style="text-align: left">
              <strong>DarkCity</strong> est une ville des ann�es 2020, o� la criminalit� est un vrai probl�me, les forces de police sont d�bord�es et donc inefficaces, elles d�cident ainsi de faire appel � des chasseurs de primes comme dans le Far West de l'�poque afin de faire baisser la criminalit�. 
              De l'autre cot�, la mafia s'organise et lance � son tour des contrats sur les t�tes de ces nouveaux justiciers.
              <p/>
              Vous, vous d�barquez juste dans cette ville chaotique, � vous de choisir quel cot� vous allez servir. La Justice ou la Criminalit�...
              <p/>
              Avant de jouer, n'oubliez pas de prendre connaissance des <a href="conditions.html">Conditions g�n�rales d'utilisation</a>
              <p/>
              Pour obtenir de l'aide sur le jeu ou avoir de plus amples informations sur celui-ci, veuillez visiter le site : <a href="http://darken33.free.fr/index.php?cat=8&amp;rub=35">http://darken33.free.fr/</a>,<br/> 
              Vous pouvez �galment nous contacter : <a href="mailto:darkcitygame@free.fr">darkcitygame@free.fr</a><br/><br/>
            </div>
          </div>
          <p/>
                    
          <div class="box" style="width:400px;">
            <div class="titlebox">Identification</div>
            <div class="text-box">
              <table class="login" cellspacing="10" cellpadding="5">
                <tr><th class="login">Pseudonyme</th><td><?php echo "<input name=\"nom\" class=\"text\" style=\"width: 200px;\" type=\"text\" maxlength=\"25\" value=\"".(isset($nom)?$nom:"")."\"/>"; ?></td></tr>
                <tr><th class="login">Mot de passe</th><td><input name="pwd" class="text" style="width: 200px;" type="password" maxlength="25" value=""/></td></tr>
	      </table>
	    </div>
	    <div class="actions-center">
	      <input name="action" class="action" type="submit" value="connecter" title="Connectez vous..."/>&nbsp;&nbsp;<span class="invisible">&nbsp;|&nbsp;</span><input name="action" class="action" type="submit" value="nouveau" title="Cr&eacute;ez un nouveau personnage" />
	    </div>
	  </div>
          <p/>
           <a href="index.php?action=oubli"/>J'ai oubli� mon mot de passe</a> | <a href="index.php?action=desinscrire"/>Je veux me d�sinscrire du jeu</a> 
	  <p/>	
          <div class="box">
            <div class="titlebox">Les 5 derni�res nouvelles de DarkCity</div>
            <div class="text-box" style="text-align: left">
              <br/>&nbsp;&nbsp; Voici les cinq derni�res news :
<?php 
  if ($newsDataBean->getNbNews() == 0) {
    echo "              Pas de nouvelles, bonnes nouvelles..."."\n";
  }
  $nb = ($newsDataBean->getNbNews() < 5 ? $newsDataBean->getNbNews() : 5);
  echo "              <ul>\n";
  for ($i = 0; $i < $nb; $i++) {
    $news = $newsDataBean->getNewsAt($i);
    echo '                <li>'.$news->getNews_titre().'</li>'."\n";
  }
  echo "              </ul>\n";
?>
            </div>        
          </div>        
	  
        </div>
      </form>
      <hr />
<?php 
  include("vue/footer.php");
  $logManager->message("login.view : debut"); 
?>
