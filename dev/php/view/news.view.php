<?php 
/**
 * login.view.php : vue correspondant � la page d'authentification
 *
 * Date    : 14/09/2012
 * Auteur  : Philippe Bousquet
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2012 Philippe Bousquet.
 *
 * Ce logiciel est distribu� selon les termes de la GNU General Public License v3.
 * License : http://www.gnu.org/copyleft/gpl.html
 */
 
  $logManager->message("login.view : debut"); 
  require("include/langage.inc.php");
  include("view/header_login.php");
?>
				
				<div class="main">
<?php
  if ($errorManager->isError()) {
?>	  
					<div class="error"><?=$errorManager->getMessage();?></div>
<?php
  }
?>  
<?php 
  if ($newsDataBean->getNbNews() == 0) {
?>	  
					<div class="box" style="text-align: center; padding: 5px; border: #CFCFCF 1px solid; background: #652020; -moz-border-radius-topleft: 8px; -webkit-border-top-left-radius: 8px; border-top-left-radius: 8px;  -moz-border-radius-bottomleft: 8px; -webkit-border-bottom-left-radius: 8px; border-bottom-left-radius: 8px; -moz-border-radius-topright: 8px; -webkit-border-top-right-radius: 8px; border-top-right-radius: 8px;  -moz-border-radius-bottomright: 8px; -webkit-border-bottom-right-radius: 8px; border-bottom-right-radius: 8px;">
						<div class="text-box" style="text-align: left">
							Pas de nouvelles, bonnes nouvelles...
						</div>        
					</div>        
<?
  }
  $nb = $newsDataBean->getNbNews();
  for ($i = 0; $i < $nb; $i++) {
    $news = $newsDataBean->getNewsAt($i);
?>
					<div class="box" style="text-align: center; padding: 5px; border: #CFCFCF 1px solid; background: #652020; -moz-border-radius-topleft: 8px; -webkit-border-top-left-radius: 8px; border-top-left-radius: 8px;  -moz-border-radius-bottomleft: 8px; -webkit-border-bottom-left-radius: 8px; border-bottom-left-radius: 8px; -moz-border-radius-topright: 8px; -webkit-border-top-right-radius: 8px; border-top-right-radius: 8px;  -moz-border-radius-bottomright: 8px; -webkit-border-bottom-right-radius: 8px; border-bottom-right-radius: 8px;">
						<div class="text-box" style="text-align: left">
							<h3><?=$news->getNews_creation()." - ".$news->getNews_titre();?></h3>
							<?=translate($news->getNews_texte());?>
						</div>        
					</div>        
<?
  }
?>  					
				</div>
			</div>				
		</div>  
<?php 
  include("view/footer_login.php");
  $logManager->message("login.view : debut"); 
?>
