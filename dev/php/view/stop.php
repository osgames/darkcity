<?php 
/**
 * login.php : vue correspondant � la page d'authentification
 *
 * Date    : 18/03/2009
 * Auteur  : Thierry Bousquet & Philippe Bousquet 
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2008-2009 Philippe Bousquet.
 * 
 * Ce logiciel est distribu� selon le termes de la GNU General Public License v2.
 */
 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>DarkCity - MMORPG par Navigateur</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <meta http-equiv="keywords" content="darkcity, mmorpg, online, free, libre, jeu" />
    <link rel="stylesheet" href="themes/default/style.css" type="text/css" />
  </head>
  <body>
  <div class="border">
    <div class="body">
<?php
  echo "      <form action=\"index.php\" method=\"get\">\n";   
?>  
        <div class="title"><img class="title" src="images/jeu/ville2.jpg" alt="DARKCITY - La ville est &agrave; toi" title=""/></div>
        <hr />
        <div class="menu">
          <input name="action" type="submit" class="menu" value="accueil"/> 
          <input name="action" type="submit" class="menu" value="news"/> 
          <input name="action" type="submit" class="menu" value="images"/> 
          <input name="action" type="submit" class="menu" value="cr&eacute;dits"/>
          <input name="action" type="submit" class="menu" value="soutien"/>
          <input name="action" type="submit" class="menu" value="contacts"/>
        </div>
      </form>
      <hr />

        <div class="main">
          <input type="hidden" name="pageCourante" value="stop" />
          <div class="box">
            <div class="titlebox">Interruption de service</div>
            <img class="image" src="images/events/travaux.gif" alt="" title="" />
            <div class="text-box" style="text-align: left; padding: 10px;">
              <p/>
              <strong>Nous sommes d�sol� mais nous devons temporairement interrompre DarkCity.</strong>
              <p/>
<!--
              En effet nous somme en train d'installer des mise � jour qui peuvent perturber le fonctionnement du jeu.<br/>
              Veuillez nous excuser de la g�ne occasionn�e, et vous reconnecter un peu plus tard. En g�n�ral ce genre de mise � jour ne prennent pas plus de 30 minutes...<br/>
-->
Suite a des irr�gularit�s perp�tr�es sur le site, celui-ci est ferm� temporairement...<br/>
En effet un bug permettait a des joueurs de faire augmenter leurs comp�tences de mani�re irraison�e...<br/>
Le fait que je ne puisse aujourd'hui, ni corriger le bug rapidement, ni d�tecter sans enqu�te les possibles tricheurs, je me vois dans l'obligationde fermer le service.<br/>
<br/>
Je tiens a pr�senter mes excuses a toutes les peronnes qui jouaient a darkcity tout simplement, dans l'esprit que j'avais voulu, libre, gratuit et responsable...<br/>
<br/>
J'esp�re r�-ouvrir le site bient�t, mais je ne peux malheureusement donner aucune garantie...<br/>
<br/>
Au pire cela aura �t� une belle aventure de 2 ans...
              <p/>
              Cordialement,<br/>
              Philippe Bousquet<br/>
	      Responsable du projet DarkCity.<br/>
              Contact : <a href="mailto:darken33.free.fr">darken33@free.fr</a><br/><br/>         
            </div>
          </div>

          <div class="box">
            <div class="titlebox">Accueil</div>
            <img class="image" src="images/armes/Arasaka_253.gif" alt="" title="" />
            <div class="text-box" style="text-align: left">
              <strong>DarkCity</strong> est une ville des ann�es 2020, o� la criminalit� est un vrai probl�me, les forces de police sont d�bord�es et donc inefficaces, elles d�cident ainsi de faire appel � des chasseurs de primes comme dans le Far West de l'�poque afin de faire baisser la criminalit�. 
              De l'autre cot�, la mafia s'organise et lance � son tour des contrats sur les t�tes de ces nouveaux justiciers.
              <p/>
              Vous, vous d�barquez juste dans cette ville chaotique, � vous de choisir quel cot� vous allez servir. La Justice ou la Criminalit�...
              <p/>
              Avant de jouer, n'oubliez pas de prendre connaissance des <a href="conditions.html">Conditions g�n�rales d'utilisation</a>
              <p/>
              Pour obtenir de l'aide sur le jeu ou avoir de plus amples informations sur celui-ci, veuillez visiter le site : <a href="http://darken33.free.fr/index.php?cat=8&amp;rub=35">http://darken33.free.fr/</a>,<br/> 
              Vous pouvez �galment nous contacter : <a href="mailto:darkcitygame@free.fr">darkcitygame@free.fr</a><br/><br/>
            </div>
          </div>
          <p/>
        </div>            
      <hr />
<?php 
  include("vue/footer.php");
?>
