<?php 
/**
 * soutiens.php : vue correspondant � la page de soutien
 *
 * Date    : 18/03/2009
 * Auteur  : Philippe Bousquet 
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2008-2009 Philippe Bousquet.
 * 
 * Ce logiciel est distribu� selon le termes de la GNU General Public License v2.
 */

 $logManager->message("page soutiens : debut"); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>DarkCity - MMORPG par Navigateur</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <meta http-equiv="keywords" content="darkcity, mmorpg, online, free, libre, jeu" />
    <link rel="stylesheet" href="themes/default/style.css" type="text/css" />
  </head>
  <body>
  <div class="border">
    <div class="body">
      <form action="index.php" method="get"> 
      <div class="title"><img class="title" src="images/jeu/ville2.jpg" alt="DARKCITY - La ville est &agrave; toi" title=""/></div>
      <hr />
      <div class="menu">
        <input type="hidden" name="pageCourante" value="news" />
        <input name="action" type="submit" class="menu" value="accueil"/> 
        <input name="action" type="submit" class="menu" value="news"/> 
        <input name="action" type="submit" class="menu" value="images"/> 
        <input name="action" type="submit" class="menu" value="cr&eacute;dits"/>
        <input name="action" type="submit" class="menu" value="soutien"/>
        <input name="action" type="submit" class="menu" value="contacts"/>
      </div>
      </form>
      <hr />
      <div class="main">
        <p/>
        <div class="box">
          <div class="titlebox">Nous soutenir</div>
          <div class="scenario-box" style="text-align: left;">
            <p/>
            <strong>DarkCity</strong> est un jeu gratuit, et il le restera !!!<br/>
            <p/>
            Nous avons cependant besoin de votre soutien, et vous pouvez le faire de plusieurs mani�res diff�rentes :<br/>
            <p/>
            <strong>1. Nous envoyer un petit email</strong><br/>
            Recevoir un retour sur ce que l'on fait est toujours gratifiant, donc si le coeur vous en dit, n'h�sitez pas � nous envoyer un email 
            pour nous faire part de vos remarques.<br/>
            Pour cela il suffit d'envoyer un email � <a href="mailto:darkcitygame@free.fr">darkcitygame@free.fr</a>
            <p/>
            <strong>2. Faire un don</strong><br/>
            Ces dons pourront servir � am�liorer encore plus et plus rapidement le service et le jeu en lui meme, 
            la premi�re chose � quoi serviront vos dons sera le financement d'un serveur afin de proposer un service 
            plus stable et plus robuste qu'actuellement.<br/>
            Pour cela il suffit de cliquer sur le bouton ci-dessous :<br/>
            <div style="text-align: center;">
	    <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
               <input type="hidden" name="cmd" value="_s-xclick" />
               <input type="hidden" name="hosted_button_id" value="2850810" />
               <input type="image" src="https://www.paypal.com/fr_FR/FR/i/btn/btn_donate_LG.gif" name="submit" alt="" />
               <img alt="" border="0" src="https://www.paypal.com/fr_FR/i/scr/pixel.gif" width="1" height="1" />
             </form>
           </div>  
            <p/>
            <strong>3. Faire connaitre le jeu</strong><br/>
            Le plus important de tout �tant de parler de ce jeu autour de vous, car plus il y aura de joueurs et plus le jeu sera interressant. En effet la grosse partie du jeu est le PvP.
            Alors si ce jeu vous plait, n'h�sitez pas � en parler autour de vous. 
	   <p/>
	   Sachez que n'importe laquelle des solutions que vous choisirez pour manifester votre soutien, nous ira droit au coeur.
	   <br/>
	   <br/>
          </div>
        </div>  
      </div>
      <hr />
<?php      
  include("vue/footer.php");
  $logManager->message("Page g�n�r�e en ".$chrono->stop()."s.");  
  $logManager->message("page soutiens : fin"); 
?>