<?php 
/**
 * header.php : ent�te des pages du jeu
 *
 * Date    : 18/03/2009
 * Auteur  : Philippe Bousquet 
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2008-2009 Philippe Bousquet.
 * 
 * Ce logiciel est distribu� selon le termes de la GNU General Public License v2.
 */

 $logManager->message("page header : debut"); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>DarkCity - MMORPG par Navigateur</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-15" />
    <meta http-equiv="keywords" content="darkcity, mmorpg, online, free, libre, jeu" />
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <meta http-equiv="expire" content="-1"/>
    <link rel="stylesheet" href="themes/default/style.css?v1.1.2" type="text/css" />
  </head>
  <body>
<?php
  $miniFicheDataBean = $sessionManager->load("miniFiche");
?>  
  <div class="border">
    <div class="body">
      <div class="title" style="text-align: left;">
        <table style="margin: 0; padding: 0; border: 0;" cellspacing="0" cellpadding="0">
          <tr>
            <td style="width: 512px; text-align: left;"><img class="title" src="images/jeu/titre.jpg?v1.1" alt="DARKCITY - La ville est &agrave; toi" title=""/></td>
            <td style="text-align: right;">
              <table style="text-align: left;">
                <tr><td colspan="3" style="border: #B0B0B0 1px solid; text-align: center"><strong><?php echo date("d/m/Y H:i:s",$dateManager->getCurrentDate()); ?></strong></td></tr>
                <tr><td style="border: #B0B0B0 1px solid;">REP : <?php echo '<span style="color: '.($miniFicheDataBean->getReputation() < 0 ? "red":"#00FF00").'" >'.abs($miniFicheDataBean->getReputation()).'</span>'; ?></td><td style="border: #B0B0B0 1px solid;">ARG : <span style="color: #FFFF00"><?php echo $miniFicheDataBean->getArgent(); ?> $</span></td><td style="border: #B0B0B0 1px solid;">VIE : <?php echo '<span style="color: '.($miniFicheDataBean->getSante() <= 25 ? "#FF0000" : ($miniFicheDataBean->getSante() <= 75 ? "#FFFF00" : "#00FF00")).'">'.$miniFicheDataBean->getSante().'/100'; ?></span></td></tr>
                <tr>
                  <td style="border: #B0B0B0 1px solid;">TIR : <span style="color: #00FF00"><?php $comp = $miniFicheDataBean->getCompetenceAt(0); echo $comp->getLevel().($comp->getBonus()<0 ? '':'+').$comp->getBonus();?></span></td>
                  <td style="border: #B0B0B0 1px solid;">BGR : <span style="color: #00FF00"><?php $comp = $miniFicheDataBean->getCompetenceAt(1); echo $comp->getLevel().($comp->getBonus()<0 ? '':'+').$comp->getBonus();?></span></td>
                  <td style="border: #B0B0B0 1px solid;">PHY : <span style="color: #00FF00"><?php $comp = $miniFicheDataBean->getCompetenceAt(2); echo $comp->getLevel().($comp->getBonus()<0 ? '':'+').$comp->getBonus();?></span></td>
                </tr>
                <tr>
                  <td style="border: #B0B0B0 1px solid;">REC : <span style="color: #00FF00"><?php $comp = $miniFicheDataBean->getCompetenceAt(3); echo $comp->getLevel().($comp->getBonus()<0 ? '':'+').$comp->getBonus();?></span></td>
                  <td style="border: #B0B0B0 1px solid;">SEC : <span style="color: #00FF00"><?php $comp = $miniFicheDataBean->getCompetenceAt(4); echo $comp->getLevel().($comp->getBonus()<0 ? '':'+').$comp->getBonus();?></span></td>
                  <td style="border: #B0B0B0 1px solid;">RPR : <span style="color: #00FF00"><?php $comp = $miniFicheDataBean->getCompetenceAt(5); echo $comp->getLevel().($comp->getBonus()<0 ? '':'+').$comp->getBonus();?></span></td>
                </tr>
                <tr>
                  <td style="border: #B0B0B0 1px solid;">PRS : <span style="color: #00FF00"><?php $comp = $miniFicheDataBean->getCompetenceAt(6); echo $comp->getLevel().($comp->getBonus()<0 ? '':'+').$comp->getBonus();?></span></td>
                  <td style="border: #B0B0B0 1px solid;">MED : <span style="color: #00FF00"><?php $comp = $miniFicheDataBean->getCompetenceAt(7); echo $comp->getLevel().($comp->getBonus()<0 ? '':'+').$comp->getBonus();?></span></td>
                  <td style="border: #B0B0B0 1px solid;">PER : <span style="color: #00FF00"><?php $comp = $miniFicheDataBean->getCompetenceAt(8); echo $comp->getLevel().($comp->getBonus()<0 ? '':'+').$comp->getBonus();?></span></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
      <hr />
<?php
  echo "      <form action=\"".$sessionManager->parseURL("index.php", "")."\" method=\"post\">\n";   
?>  
        <div class="menu">
<?php
  $logManager->message("page header : bouton fiche actif = ".$menuDataBean->isBoutonFicheActif());
  echo '          <input name="pageCourante" type="hidden" value="'.$pageCourante.'"/>';
  echo '          <input name="action" type="submit" class="menu'.($menuDataBean->isBoutonFicheActif() ? '' : '_disabled').'" value="fiche" '.($menuDataBean->isBoutonFicheActif() ? '' : 'disabled="disabled"').'/>'."\n";
  echo '          <input name="action" type="submit" class="menu'.($menuDataBean->isBoutonMessagesActif() ? '' : '_disabled').'" value="messages" '.($menuDataBean->isBoutonMessagesActif() ? '' : 'disabled="disabled"').'/>'."\n";
  echo '          <input name="action" type="submit" class="menu'.($menuDataBean->isBoutonVilleActif() ? '' : '_disabled').'" value="ville" '.($menuDataBean->isBoutonVilleActif() ? '' : 'disabled="disabled"').'/>'."\n";
  echo '          <input name="action" type="submit" class="menu'.($menuDataBean->isBoutonPlanqueActif() ? '' : '_disabled').'" value="planque" '.($menuDataBean->isBoutonPlanqueActif() ? '' : 'disabled="disabled"').'/>'."\n";
  echo '          <input name="action" type="submit" class="menu'.($menuDataBean->isBoutonHighScoreActif() ? '' : '_disabled').'" value="high-score" '.($menuDataBean->isBoutonHighScoreActif() ? '' : 'disabled="disabled"').'/>'."\n";
  echo '          <input name="action" type="submit" class="menu'.($menuDataBean->isBoutonQuitterActif() ? '' : '_disabled').'" value="quitter" '.($menuDataBean->isBoutonQuitterActif() ? '' : 'disabled="disabled"').'/>'."\n";
?>  
        </div>
      </form> 
      <hr />
<?php
 $logManager->message("page header : fin"); 
?>      