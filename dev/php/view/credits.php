<?php 
/**
 * credits.php : vue correspondant � la page des cr�dits & remerciments
 *
 * Date    : 18/03/2009
 * Auteur  : Philippe Bousquet 
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2008-2009 Philippe Bousquet.
 * 
 * Ce logiciel est distribu� selon le termes de la GNU General Public License v2.
 */

 $logManager->message("page credits : debut"); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>DarkCity - MMORPG par Navigateur</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <meta http-equiv="keywords" content="darkcity, mmorpg, online, free, libre, jeu" />
    <link rel="stylesheet" href="themes/default/style.css" type="text/css" />
  </head>
  <body>
  <div class="border">
    <div class="body">
      <form action="index.php" method="get"> 
      <div class="title"><img class="title" src="images/jeu/ville2.jpg" alt="DARKCITY - La ville est &agrave; toi" title=""/></div>
      <hr />
      <div class="menu">
        <input type="hidden" name="pageCourante" value="news" />
        <input name="action" type="submit" class="menu" value="accueil"/> 
        <input name="action" type="submit" class="menu" value="news"/> 
        <input name="action" type="submit" class="menu" value="images"/> 
        <input name="action" type="submit" class="menu" value="cr&eacute;dits"/>
        <input name="action" type="submit" class="menu" value="soutien"/>
        <input name="action" type="submit" class="menu" value="contacts"/>
      </div>
      </form>
      <hr />
      <div class="main">
        <p/>
        <div class="box">
          <div class="titlebox">A propos de DarkCity</div>
          <div class="scenario-box" style="text-align: left;">
            <p/>
            <strong>DarkCity</strong> version <?php echo $VERSION; ?><br/>
            <?php echo $COPY; ?><br/>
            D'apr�s une id�e originale de Philippe Bousquet.<br/>
            Ce logiciel est plac� sous licence Gnu General Public License v2.
            <p/>
            <strong>Communication</strong><br/>
            Communication externe : Thierry Bousquet<br/>
            Communication interne : Philippe Bousquet<br/>
            <p/>
            <strong>Conception du jeu</strong><br/>
            R�gles et Actions du jeu : Thierry Bousquet &amp; Philippe Bousquet<br />
            Sc�narios : Patricia Larive &amp; Philippe Bousquet
            <p/>
            <strong>Equipe de d�veloppement</strong><br/>
            Thierry Bousquet &amp; Philippe Bousquet<br/>
            <p/>
            <strong>Graphisme</strong><br/>
            Plusieurs images sont originaires du web :
            <ul>
              <li><a href="http://www.barrysclipart.com/">http://www.barrysclipart.com/</a></li>
              <li><a href="http://openclipart.org/">http://openclipart.org/</a></li>
              <li><a href="http://cyberpunk.liber-mundi.org/">http://cyberpunk.liber-mundi.org/</a></li>
            </ul>
            Les retouches et autres images ont �t� r�alis�es par Thierry Bousquet &amp; Philippe Bousquet.<br />
            Le th&egrave;me du jeu a �t� r�alis� par Denis Julien.
            <p/>
            <strong>Relecture</strong><br/>
            L'�tape de relecture de bon nombre de documents a �t� r�alis�e par Marion Hittos
            <p/>
            <strong>Tests</strong><br/>
            L'�quipe d'alpha testeurs a �t� compos�e des personnes suivantes :<br/>
            Marion Hittos,<br/>
            Luc Morpho,<br/>
            Christophe Blondel,<br/>
            Olivier Guerraz,<br/>
            Patricia Larive,<br/>
            Bruno Larive,<br/>
            Andr� Bouazis,<br/>
            St�phane Blondel,<br/>
            S�bastien Vidal,<br/>
            Thierry Bousquet,<br/>
            Philippe Bousquet.<br/>
            <p/>
            L'�quipe de <strong>DarkCity</strong> remercie l'ensemble des personnes qui participent de pr�s 
            ou de loin au d�veloppement de ce jeu, que ce soit par leurs commentaires, id�es ou critiques...
            <p/>
            <strong>DarkCity</strong> est enti�rement con�u et d�velopp� gr�ce � des logiciels libres tels que <a href="http://httpd.apache.org/">Apache</a>, <a href="http://www.mysql.fr/">MySQL</a>, <a href="http://www.php.net/">PHP</a>, <a href="http://www.easyphp.org/">EasyPHP</a>, <a href="http://argouml.tigris.org/">ArgoUML</a>, <a href="http://OpenOffice.org/">OpenOffice.org</a>, <a href="http://www.gimp.org/">The Gimp</a>, <a href="http://www.phpmyadmin.net/">phpMyAdmin</a>, <a href="http://fabforce.net/dbdesigner4/">DBDesigner</a>, <a href="http://www.titaniclinux.net/daogen/">DAOGen</a>, <a href="http://www.jext.org/">Jext</a>,...
            <p/>
            Enfin nous tenons � remercier toutes les personnes qui nous soutiennent dans ce projet.
            <br/>
            <br/>
          </div>
        </div>  
      </div>
      <hr />
<?php 
  include("vue/footer.php");
  $logManager->message("Page g�n�r�e en ".$chrono->stop()."s.");  
  $logManager->message("page credits : fin"); 
?>