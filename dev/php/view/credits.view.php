<?php 
/**
 * credits.php : vue correspondant à la page des crédits & remerciments
 *
 * Date    : 18/03/2009
 * Auteur  : Philippe Bousquet 
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2008-2009 Philippe Bousquet.
 * 
 * Ce logiciel est distribué selon le termes de la GNU General Public License v2.
 */

 $logManager->message("page credits : debut"); 
 include_once("view/header_login.php"); 
?>
				<div class="main">
					<p/>
					<div class="box" style="text-align: center; padding: 5px; border: #CFCFCF 1px solid; background: #652020; -moz-border-radius-topleft: 8px; -webkit-border-top-left-radius: 8px; border-top-left-radius: 8px;  -moz-border-radius-bottomleft: 8px; -webkit-border-bottom-left-radius: 8px; border-bottom-left-radius: 8px; -moz-border-radius-topright: 8px; -webkit-border-top-right-radius: 8px; border-top-right-radius: 8px;  -moz-border-radius-bottomright: 8px; -webkit-border-bottom-right-radius: 8px; border-bottom-right-radius: 8px;">
						<h3>A propos de DarkCity</h3>
						<div class="scenario-box" style="text-align: left;">
							<p/>
							<strong>DarkCity</strong> version <?php echo $VERSION; ?><br/>
							<?php echo $COPY; ?>
							<p/>
							<strong>Id&eacute;e originale</strong><br/>
							Ce jeu est basé sur l'univers de <strong>Cyberpunk 2020</strong> de Talsorian Games<br/>
							Il se base également sur plusieurs concepts de la première version du jeu développé par Thierry et Philippe Bousquet entre 2008 et 2010
							<p/>
							<strong>Conception et d&eacute;veloppement du jeu</strong><br/>
							Ce jeu est d&eacute;velopp&eacute; par Philippe Bousquet sur son temps libre, et n'a pa vocation à devenir lucratif. Dans cette optique l&agrave;,
							il est concu et imagin&eacute; en suivant les concepts du logiciel et de la culture libre. Toute personne d&eacute;sirant participer est la bienvenue.
							<p/>
							<strong>Graphisme</strong><br/>
							Plusieurs images sont originaires du web :
							<ul>
								<li><a href="http://www.barrysclipart.com/">http://www.barrysclipart.com/</a></li>
								<li><a href="http://openclipart.org/">http://openclipart.org/</a></li>
								<li><a href="http://cyberpunk.liber-mundi.org/">http://cyberpunk.liber-mundi.org/</a></li>
							</ul>
							Les retouches et autres images ont été réalisées par Thierry Bousquet &amp; Philippe Bousquet.<br />
							Le th&egrave;me du jeu a été réalisé par Denis Julien.
							<p/>
							<strong>DarkCity</strong> est entièrement conçu et développé grâce à des logiciels libres tels que <a href="http://httpd.apache.org/">Apache</a>, <a href="http://www.mysql.fr/">MySQL</a>, <a href="http://www.php.net/">PHP</a>, <a href="http://www.easyphp.org/">EasyPHP</a>, <a href="http://argouml.tigris.org/">ArgoUML</a>, <a href="http://OpenOffice.org/">OpenOffice.org</a>, <a href="http://www.gimp.org/">The Gimp</a>, <a href="http://www.phpmyadmin.net/">phpMyAdmin</a>, <a href="http://fabforce.net/dbdesigner4/">DBDesigner</a>, <a href="http://www.titaniclinux.net/daogen/">DAOGen</a>, <a href="http://www.jext.org/">Jext</a>,...
							<p/>
							Enfin nous tenons à remercier toutes les personnes qui nous soutiennent dans ce projet.
							<br/>
							<br/>
						</div>
					</div>  
				</div>
			</div>
      </div>
      <hr />
<?php 
  include("view/footer_login.php");
  $logManager->message("page credits : fin"); 
?>
