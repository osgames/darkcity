<?php 
/**
 * mairie.php : vue correspondant à la page mairie
 *
 * Date    : 21/03/2009
 * Auteur  : Thierry Bousquet 
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2008-2009 Philippe Bousquet.
 * 
 * Ce logiciel est distribué selon le termes de la GNU General Public License v2.
 */

  $logManager->message("page contacts : debut"); 
  include_once("view/header_login.php"); 
?>
				<div class="main">
<!--
					<div class="box" style="text-align: center; padding: 5px; border: #CFCFCF 1px solid; background: #652020; -moz-border-radius-topleft: 8px; -webkit-border-top-left-radius: 8px; border-top-left-radius: 8px;  -moz-border-radius-bottomleft: 8px; -webkit-border-bottom-left-radius: 8px; border-bottom-left-radius: 8px; -moz-border-radius-topright: 8px; -webkit-border-top-right-radius: 8px; border-top-right-radius: 8px;  -moz-border-radius-bottomright: 8px; -webkit-border-bottom-right-radius: 8px; border-bottom-right-radius: 8px;">
						<h3>La messagerie interne</h3>
						<div class="action-box" style="text-align: left;">
							<img class="image" src="images/actions/mail.gif" alt="[Messagerie]" title=""/><br/>Vous avez à votre disposition une messagerie interne au jeu.<br/>
							Celle-ci vous permet de converser avec un autre joueur, cette discussion est privée et n'est pas visible par les autres joueurs. Merci de rester courtois lorsque vous vous adressez à une autre personne et de respecter la "Netiquette".<br/>
						</div>
						<p/>
					</div>  

					<div class="box" style="text-align: center; padding: 5px; border: #CFCFCF 1px solid; background: #652020; -moz-border-radius-topleft: 8px; -webkit-border-top-left-radius: 8px; border-top-left-radius: 8px;  -moz-border-radius-bottomleft: 8px; -webkit-border-bottom-left-radius: 8px; border-bottom-left-radius: 8px; -moz-border-radius-topright: 8px; -webkit-border-top-right-radius: 8px; border-top-right-radius: 8px;  -moz-border-radius-bottomright: 8px; -webkit-border-bottom-right-radius: 8px; border-bottom-right-radius: 8px;">
						<h3>Le tchat</h3>
						<div class="action-box" style="text-align: left;">
							<img class="image" src="images/actions/itchat.gif" alt="[Tchat]" title=""/><br/>Vous avez également à votre disposition un tchat.<br/>
							Cela vous permet d'avoir des discussions avec l'ensemble des joueurs connectés, tout ce que vous dites est alors visible pendant une heure par tous les autres joueurs. Merci de rester courtois lorsque vous vous exprimez sur le tchat et de respecter la "Netiquette".<br/>
						</div>
						<p/>
					</div>  
-->  
					<div class="box" style="text-align: center; padding: 5px; border: #CFCFCF 1px solid; background: #652020; -moz-border-radius-topleft: 8px; -webkit-border-top-left-radius: 8px; border-top-left-radius: 8px;  -moz-border-radius-bottomleft: 8px; -webkit-border-bottom-left-radius: 8px; border-bottom-left-radius: 8px; -moz-border-radius-topright: 8px; -webkit-border-top-right-radius: 8px; border-top-right-radius: 8px;  -moz-border-radius-bottomright: 8px; -webkit-border-bottom-right-radius: 8px; border-bottom-right-radius: 8px;">
						<h3>Le site du projet</h3>
						<div class="action-box" style="text-align: left;">
							<img class="image" src="images/actions/site.gif" alt="[Site]" title=""/><br/><strong>DarkCity</strong> est un projet Libre, il possède donc une page sur Sourceforge.<br/>
							Vous pourrez y trouver diverses informations telles que : une présentation du projet, le dépôt svn du projet, trackeur de bugs...<br/>
							Son adresse : <a href="http://darkcity.sourceforge.net/" target="_blank">http://darkcity.sourceforge.net/</a>
						</div>
						<p/>
					</div>  

					<div class="box" style="text-align: center; padding: 5px; border: #CFCFCF 1px solid; background: #652020; -moz-border-radius-topleft: 8px; -webkit-border-top-left-radius: 8px; border-top-left-radius: 8px;  -moz-border-radius-bottomleft: 8px; -webkit-border-bottom-left-radius: 8px; border-bottom-left-radius: 8px; -moz-border-radius-topright: 8px; -webkit-border-top-right-radius: 8px; border-top-right-radius: 8px;  -moz-border-radius-bottomright: 8px; -webkit-border-bottom-right-radius: 8px; border-bottom-right-radius: 8px;">
						<h3>Participer aux discussions sur le futur de DarkCity</h3>
						<div class="action-box" style="text-align: left;">
							<img class="image" src="images/actions/mailing_list.gif" alt="[Mailing List]" title=""/><br/>Vous désirez participer aux discussions sur les futures evolutions du jeu ?<br/>
							Vous avez pour cela la possibilité de vous inscrire sur notre mailing list. Vous serez alors informé, et vous pourrez participer aux débats concerant le jeu.<br/>
							Pour s'inscrire : <a href="mailto:darkcitygame-request@ml.free.fr?subject=subscribe" target="_blank">darkcitygame-request@ml.free.fr?subject=subscribe</a><br/>
						</div>
						<p/>
					</div>  
  
					<div class="box" style="text-align: center; padding: 5px; border: #CFCFCF 1px solid; background: #652020; -moz-border-radius-topleft: 8px; -webkit-border-top-left-radius: 8px; border-top-left-radius: 8px;  -moz-border-radius-bottomleft: 8px; -webkit-border-bottom-left-radius: 8px; border-bottom-left-radius: 8px; -moz-border-radius-topright: 8px; -webkit-border-top-right-radius: 8px; border-top-right-radius: 8px;  -moz-border-radius-bottomright: 8px; -webkit-border-bottom-right-radius: 8px; border-bottom-right-radius: 8px;">
						<h3>DarkCity sur Facebook</h3>
						<div class="action-box" style="text-align: left;">
							<img class="image" src="images/actions/facebook.gif" alt="[Site]" title=""/><br/>Un groupe <strong>DarkCity</strong> existe sur facebook.<br/> 
							Vous pouvez rejoindre ce groupe et participer ainsi à la promotion du jeu, et nous contacter au travers de cet outil.<br/>
							L'adresse du groupe : <a href="http://www.facebook.com/group.php?gid=50177858187" target="_blank">http://www.facebook.com/group.php?gid=50177858187</a><br/>
						</div>
						<p/>
					</div>    
    
					<div class="box" style="text-align: center; padding: 5px; border: #CFCFCF 1px solid; background: #652020; -moz-border-radius-topleft: 8px; -webkit-border-top-left-radius: 8px; border-top-left-radius: 8px;  -moz-border-radius-bottomleft: 8px; -webkit-border-bottom-left-radius: 8px; border-bottom-left-radius: 8px; -moz-border-radius-topright: 8px; -webkit-border-top-right-radius: 8px; border-top-right-radius: 8px;  -moz-border-radius-bottomright: 8px; -webkit-border-bottom-right-radius: 8px; border-bottom-right-radius: 8px;">
						<h3>darkcitygame@free.fr</h3>
						<div class="action-box" style="text-align: left;">
							<img class="image" src="images/actions/mail.gif" alt="[Contact]" title=""/><br/>Vous avez une question ? vous désirez une information particulière ?.<br/>
							Vous pouvez contacter l'équipe qui gère et développe <strong>DarkCity</strong> par email à l'adresse <a href="mailto:darkcitygame@free.fr">darkcitygame@free.fr</a>. Nous vous répondrons dans les meilleurs délais.<br/>
						</div>
						<p/>
					</div>  

				</div>
			</div>
		</div>
		<hr />
<?php      
  include("view/footer_login.php");
  $logManager->message("page contacts : fin"); 
?>
