<?php 
/**
 * oubli.php : vue correspondant � la page d'oubli du mot de passe
 *
 * Date    : 31/05/2009
 * Auteur  : Philippe Bousquet 
 * Version : 1.0
 * Contact : <darkcitygame@free.fr>
 *
 * Copyright (c) 2008-2009 Philippe Bousquet.
 * 
 * Ce logiciel est distribu� selon le termes de la GNU General Public License v2.
 */
 
  $logManager->message("page login : debut"); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
  <head>
    <title>DarkCity - MMORPG par Navigateur</title>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
    <meta http-equiv="keywords" content="darkcity, mmorpg, online, free, libre, jeu" />
    <link rel="stylesheet" href="themes/default/style.css" type="text/css" />
  </head>
  <body>
  <div class="border">
    <div class="body">
<?php
  echo "      <form action=\"index.php\" method=\"get\">\n";   
?>  
        <div class="title"><img class="title" src="images/jeu/ville2.jpg" alt="DARKCITY - La ville est &agrave; toi" title=""/></div>
        <hr />
        <div class="menu">
          <input name="action" type="submit" class="menu" value="accueil"/> 
          <input name="action" type="submit" class="menu" value="news"/> 
          <input name="action" type="submit" class="menu" value="images"/> 
          <input name="action" type="submit" class="menu" value="cr&eacute;dits"/>
          <input name="action" type="submit" class="menu" value="soutien"/>
          <input name="action" type="submit" class="menu" value="contacts"/>
	        </div>
      </form>
      <hr />

<?php
  echo "      <form action=\"".$sessionManager->parseURL("index.php", "")."\" method=\"post\">\n";   
?>  
        <div class="main">
          <input type="hidden" name="pageCourante" value="oubli" />
<?php
  if ($errorManager->isError()) {
    echo "          <div class=\"error\">".$errorManager->getMessage()."</div>\n";
  }
?>

          <div class="box">
            <div class="titlebox">Proc�dure de r�cup�ration du mot de passe</div>
            <div class="text-box" style="text-align: left">
            Vous avez oubli�, votre mot de passe, il vous suffit d'entrer votre Pseudonyme et votre Email, nous allons vous reg�n�rer et vous renvoyez un nouveau mote de passe.
            N'oubliez pas de le changer lors de votre prochaine connexion.
            </div>
          </div>
          <p/>
                    
          <div class="box" style="width:400px;">
            <div class="titlebox">Informations Joueur</div>
            <div class="text-box">
              <table class="login" cellspacing="10" cellpadding="5">
                <tr><th class="login">Pseudonyme</th><td><?php echo "<input name=\"nom\" class=\"text\" style=\"width: 200px;\" type=\"text\" maxlength=\"25\" value=\"".(isset($nom)?$nom:"")."\"/>"; ?></td></tr>
                <tr><th class="login">Email</th><td><input name="email" class="text" style="width: 200px;" type="text" value=""/></td></tr>
	      </table>
	    </div>
	    <div class="actions-center">
	      <input name="action" class="action" type="submit" value="reg�n�rer" title="Reg�n�rer mon mot de passe"/>
	    </div>
	  </div>
        </div>
      </form>
      <hr />
<?php 
  include("vue/footer.php");
  $logManager->message("Page g�n�r�e en ".$chrono->stop()."s."); 
  $logManager->message("page login : fin");
?>