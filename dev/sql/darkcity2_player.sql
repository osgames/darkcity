-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le : Sam 08 Septembre 2012 à 13:22
-- Version du serveur: 5.5.24
-- Version de PHP: 5.3.10-1ubuntu3.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `darkcity2`
--

-- --------------------------------------------------------

--
-- Structure de la table `darkcity2_player`
--

DROP TABLE IF EXISTS `darkcity2_player`;
CREATE TABLE `darkcity2_player` (
  `player_nickname` varchar(20) NOT NULL,
  `payer_email` varchar(255) NOT NULL,
  `player_REP` int(11) NOT NULL,
  `player_ENERGY` int(11) NOT NULL,
  `player_ARG` int(11) NOT NULL,
  `player_REF` int(11) NOT NULL,
  `player_CON` int(11) NOT NULL,
  `player_PER` int(11) NOT NULL,
  `player_INT` int(11) NOT NULL,
  `player_EMP` int(11) NOT NULL,
  `player_TEC` int(11) NOT NULL,
  `player_VIE` int(11) NOT NULL,
  PRIMARY KEY (`player_nickname`),
  KEY `payer_email` (`payer_email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
